/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

/**
 *
 * @author masiya
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EventDetails implements Serializable {

    @JsonProperty("id")
    private int id;
    @JsonProperty("type")
    private String type;
    @JsonProperty("title")
    private String title;
    @JsonProperty("date")
    private String date;
    @JsonProperty("time")
    private String time;
    @JsonProperty("remaining")
    private int remaining;
    @JsonProperty("is_available")
    private boolean is_available;
    @JsonProperty("category_name")
    private String category_name;
    @JsonProperty("map")
    private Map map;
    @JsonProperty("description")
    private String description;
    @JsonProperty("cover")
    private String cover;
    @JsonProperty("gallery")
    private ImageDetails[] gallery;
    @JsonProperty("days")

    private Days[] days;
    @JsonProperty("related_events")
    private Events[] related_events;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return the time
     */
    public String getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * @return the remaining
     */
    public int getRemaining() {
        return remaining;
    }

    /**
     * @param remaining the remaining to set
     */
    public void setRemaining(int remaining) {
        this.remaining = remaining;
    }

    /**
     * @return the is_available
     */
    public boolean isIs_available() {
        return is_available;
    }

    /**
     * @param is_available the is_available to set
     */
    public void setIs_available(boolean is_available) {
        this.is_available = is_available;
    }

    /**
     * @return the category_name
     */
    public String getCategory_name() {
        return category_name;
    }

    /**
     * @param category_name the category_name to set
     */
    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    /**
     * @return the map
     */
    public Map getMap() {
        return map;
    }

    /**
     * @param map the map to set
     */
    public void setMap(Map map) {
        this.map = map;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the cover
     */
    public String getCover() {
        return cover;
    }

    /**
     * @param cover the cover to set
     */
    public void setCover(String cover) {
        this.cover = cover;
    }

    /**
     * @return the gallery
     */
    public ImageDetails[] getGallery() {
        return gallery;
    }

    /**
     * @param gallery the gallery to set
     */
    public void setGallery(ImageDetails[] gallery) {
        this.gallery = gallery;
    }

    /**
     * @return the related_events
     */
    public Events[] getRelated_events() {
        return related_events;
    }

    /**
     * @param related_events the related_events to set
     */
    public void setRelated_events(Events[] related_events) {
        this.related_events = related_events;
    }

    /**
     * @return the days
     */
    public Days[] getDays() {
        return days;
    }

    /**
     * @param days the days to set
     */
    public void setDays(Days[] days) {
        this.days = days;
    }

}
