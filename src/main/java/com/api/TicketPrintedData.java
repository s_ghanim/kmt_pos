/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

/**
 *
 * @author masiya
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TicketPrintedData implements Serializable {

    @JsonProperty("eventTitle")
    private String eventTitle = "";
    @JsonProperty("eventLocName")
    private String eventLocName = "";
    @JsonProperty("PosEmployeeID")
    private String PosEmployeeID = "";
    @JsonProperty("ticket_type_name")

    private String ticket_type_name = "";
    @JsonProperty("ticket_age_group_name")
    private String ticket_age_group_name = "";
    @JsonProperty("ticket_price")
    private String ticket_price = "";
    @JsonProperty("ticket_id")
    private String ticket_id = "";
    @JsonProperty("ticket_qrcode_url")
    private String ticket_qrcode_url = "";
    @JsonProperty("validity_date")
    private String validity_date = "";
    @JsonProperty("printerName")
    private String printerName = "";
    @JsonProperty("booking_id")
    private String booking_id = "";
    
    @JsonProperty("noOfDays")
    private String noOfDays = "";

    
    /**
     * @return the eventTitle
     */
    public String getEventTitle() {
        return eventTitle;
    }

    /**
     * @param eventTitle the eventTitle to set
     */
    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    /**
     * @return the eventLocName
     */
    public String getEventLocName() {
        return eventLocName;
    }

    /**
     * @param eventLocName the eventLocName to set
     */
    public void setEventLocName(String eventLocName) {
        this.eventLocName = eventLocName;
    }

    /**
     * @return the PosEmployeeID
     */
    public String getPosEmployeeID() {
        return PosEmployeeID;
    }

    /**
     * @param PosEmployeeID the PosEmployeeID to set
     */
    public void setPosEmployeeID(String PosEmployeeID) {
        this.PosEmployeeID = PosEmployeeID;
    }

    /**
     * @return the ticket_type_name
     */
    public String getTicket_type_name() {
        return ticket_type_name;
    }

    /**
     * @param ticket_type_name the ticket_type_name to set
     */
    public void setTicket_type_name(String ticket_type_name) {
        this.ticket_type_name = ticket_type_name;
    }

    /**
     * @return the ticket_age_group_name
     */
    public String getTicket_age_group_name() {
        return ticket_age_group_name;
    }

    /**
     * @param ticket_age_group_name the ticket_age_group_name to set
     */
    public void setTicket_age_group_name(String ticket_age_group_name) {
        this.ticket_age_group_name = ticket_age_group_name;
    }

    /**
     * @return the ticket_price
     */
    public String getTicket_price() {
        return ticket_price;
    }

    /**
     * @param ticket_price the ticket_price to set
     */
    public void setTicket_price(String ticket_price) {
        this.ticket_price = ticket_price;
    }

    /**
     * @return the ticket_id
     */
    public String getTicket_id() {
        return ticket_id;
    }

    /**
     * @param ticket_id the ticket_id to set
     */
    public void setTicket_id(String ticket_id) {
        this.ticket_id = ticket_id;
    }

    /**
     * @return the ticket_qrcode_url
     */
    public String getTicket_qrcode_url() {
        return ticket_qrcode_url;
    }

    /**
     * @param ticket_qrcode_url the ticket_qrcode_url to set
     */
    public void setTicket_qrcode_url(String ticket_qrcode_url) {
        this.ticket_qrcode_url = ticket_qrcode_url;
    }

    /**
     * @return the validity_date
     */
    public String getValidity_date() {
        return validity_date;
    }

    /**
     * @param validity_date the validity_date to set
     */
    public void setValidity_date(String validity_date) {
        this.validity_date = validity_date;
    }

    /**
     * @return the printerName
     */
    public String getPrinterName() {
        return printerName;
    }

    /**
     * @param printerName the printerName to set
     */
    public void setPrinterName(String printerName) {
        this.printerName = printerName;
    }

    /**
     * @return the booking_id
     */
    public String getBooking_id() {
        return booking_id;
    }

    /**
     * @param booking_id the booking_id to set
     */
    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    /**
     * @return the noOfDays
     */
    public String getNoOfDays() {
        return noOfDays;
    }

    /**
     * @param noOfDays the noOfDays to set
     */
    public void setNoOfDays(String noOfDays) {
        this.noOfDays = noOfDays;
    }

  
}
