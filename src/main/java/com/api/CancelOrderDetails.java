/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author masiya
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CancelOrderDetails {

    @JsonProperty("status_id")
    private String status_id;

    @JsonProperty("status_message")
    private String status_message;

    /**
     * @return the status_id
     */
    public String getStatus_id() {
        return status_id;
    }

    /**
     * @param status_id the status_id to set
     */
    public void setStatus_id(String status_id) {
        this.status_id = status_id;
    }

    /**
     * @return the status_message
     */
    public String getStatus_message() {
        return status_message;
    }

    /**
     * @param status_message the status_message to set
     */
    public void setStatus_message(String status_message) {
        this.status_message = status_message;
    }

}
