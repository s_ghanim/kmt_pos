/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 * @author masiya
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorKmt implements Serializable {

    @JsonProperty("code")
    private int code;
    @JsonProperty("message")
    private String message;

    /**
     * @return the code
     */
    public int getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    @JsonProperty("code")
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }
}
