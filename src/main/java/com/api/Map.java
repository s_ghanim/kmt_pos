/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

/**
 *
 * @author masiya
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Map implements Serializable {

    @JsonProperty("location_color")
    private String location_color;
    @JsonProperty("location_name")
    private String location_name;
    @JsonProperty("image")
    private String image;
    @JsonProperty("guid")
    private String guid;

    /**
     * @return the location_color
     */
    public String getLocation_color() {
        return location_color;
    }

    /**
     * @param location_color the location_color to set
     */
    public void setLocation_color(String location_color) {
        this.location_color = location_color;
    }

    /**
     * @return the location_name
     */
    public String getLocation_name() {
        return location_name;
    }

    /**
     * @param location_name the location_name to set
     */
    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    /**
     * @return the image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image the image to set
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return the guid
     */
    public String getGuid() {
        return guid;
    }

    /**
     * @param guid the guid to set
     */
    public void setGuid(String guid) {
        this.guid = guid;
    }
}
