/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

/**
 *
 * @author masiya
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Events implements Serializable {

    @JsonProperty("id")
    private int id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("cover")
    private String cover;
    @JsonProperty("date")
    private String date;
    @JsonProperty("type")
    private String type;
    @JsonProperty("location_name")
    private String location_name;
    @JsonProperty("location_color")
    private String location_color;

    @JsonProperty("is_available")
    private boolean is_available;
    @JsonProperty("remaining")
    private int remaining;

    /**
     * @return the id
     */
    public int getId() {

        return id;
    }

    /**
     * @param id the id to set
     */
    @JsonProperty("id")
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the cover
     */
    public String getCover() {
        return cover;
    }

    /**
     * @param cover the cover to set
     */
    @JsonProperty("cover")
    public void setCover(String cover) {
        this.cover = cover;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the location_name
     */
    public String getLocation_name() {
        return location_name;
    }

    /**
     * @param location_name the location_name to set
     */
    @JsonProperty("location_name")
    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    /**
     * @return the location_color
     */
    public String getLocation_color() {
        return location_color;
    }

    /**
     * @param location_color the location_color to set
     */
    @JsonProperty("location_color")
    public void setLocation_color(String location_color) {
        this.location_color = location_color;
    }

    /**
     * @return the is_available
     */
    public boolean isIs_available() {
        return is_available;
    }

    /**
     * @param is_available the is_available to set
     */
    @JsonProperty("is_available")
    public void setIs_available(boolean is_available) {
        this.is_available = is_available;
    }

    /**
     * @return the remaining
     */
    public int getRemaining() {
        return remaining;
    }

    /**
     * @param remaining the remaining to set
     */
    @JsonProperty("remaining")
    public void setRemaining(int remaining) {
        this.remaining = remaining;
    }

}
