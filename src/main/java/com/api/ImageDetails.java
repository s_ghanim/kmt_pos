/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

/**
 *
 * @author masiya
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ImageDetails implements Serializable {

    @JsonProperty("type")
    private String type;
    @JsonProperty("url_thump")
    private String url_thump;
    @JsonProperty("url_full")
    private String url_full;

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the url_thump
     */
    public String getUrl_thump() {
        return url_thump;
    }

    /**
     * @param url_thump the url_thump to set
     */
    public void setUrl_thump(String url_thump) {
        this.url_thump = url_thump;
    }

    /**
     * @return the url_full
     */
    public String getUrl_full() {
        return url_full;
    }

    /**
     * @param url_full the url_full to set
     */
    public void setUrl_full(String url_full) {
        this.url_full = url_full;
    }

}
