/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

/**
 *
 * @author masiya
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TicketType implements Serializable {

    @JsonProperty("type_id")
    private int type_id;
    @JsonProperty("ticket_id")
    private int ticket_id;
    @JsonProperty("age_group_name")
    private String age_group_name;
    @JsonProperty("age_group_id")
    private int age_group_id;
    @JsonProperty("remaining")
    private int remaining = 0;
    @JsonProperty("price")
    private float price = 0;
    @JsonProperty("max_quantity")
    private int max_quantity = 0;
    @JsonProperty("quantity")
    private int quantity = 0;
    @JsonProperty("type_name")
    private String type_name;
    @JsonProperty("package_name")
    private String package_name;
    @JsonProperty("ticket_price")
    private String ticket_price;
    @JsonProperty("print_details")
    private PrintDetails[] print_details;
    @JsonProperty("start_date")
    private String start_date;
    @JsonProperty("end_date")
    private String end_date;

    /**
     * @return the type_id
     */
    public int getType_id() {
        return type_id;
    }

    /**
     * @param type_id the type_id to set
     */
    public void setType_id(int type_id) {
        this.type_id = type_id;
    }

    /**
     * @return the ticket_id
     */
    public int getTicket_id() {
        return ticket_id;
    }

    /**
     * @param ticket_id the ticket_id to set
     */
    public void setTicket_id(int ticket_id) {
        this.ticket_id = ticket_id;
    }

    /**
     * @return the age_group_name
     */
    public String getAge_group_name() {
        return age_group_name;
    }

    /**
     * @param age_group_name the age_group_name to set
     */
    public void setAge_group_name(String age_group_name) {
        this.age_group_name = age_group_name;
    }

    /**
     * @return the age_group_id
     */
    public int getAge_group_id() {
        return age_group_id;
    }

    /**
     * @param age_group_id the age_group_id to set
     */
    public void setAge_group_id(int age_group_id) {
        this.age_group_id = age_group_id;
    }

    /**
     * @return the remaining
     */
    public int getRemaining() {
        return remaining;
    }

    /**
     * @param remaining the remaining to set
     */
    public void setRemaining(int remaining) {
        this.remaining = remaining;
    }

    /**
     * @return the price
     */
    public float getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(float price) {
        this.price = price;
    }

    /**
     * @return the max_quantity
     */
    public int getMax_quantity() {
        return max_quantity;
    }

    /**
     * @param max_quantity the max_quantity to set
     */
    public void setMax_quantity(int max_quantity) {
        this.max_quantity = max_quantity;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the type_name
     */
    public String getType_name() {
        return type_name;
    }

    /**
     * @param type_name the type_name to set
     */
    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    /**
     * @return the print_details
     */
    public PrintDetails[] getPrint_details() {
        return print_details;
    }

    /**
     * @param print_details the print_details to set
     */
    public void setPrint_details(PrintDetails[] print_details) {
        this.print_details = print_details;
    }

    /**
     * @param start_date the start_date to set
     */
    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    /**
     * @return the end_date
     */
    public String getEnd_date() {
        return end_date;
    }

    /**
     * @param end_date the end_date to set
     */
    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    /**
     * @return the start_date
     */
    public String getStart_date() {
        return start_date;
    }

    /**
     * @return the package_name
     */
    public String getPackage_name() {
        return package_name;
    }

    /**
     * @param package_name the package_name to set
     */
    public void setPackage_name(String package_name) {
        this.package_name = package_name;
    }

    /**
     * @return the ticket_price
     */
    public String getTicket_price() {
        return ticket_price;
    }

    /**
     * @param ticket_price the ticket_price to set
     */
    public void setTicket_price(String ticket_price) {
        this.ticket_price = ticket_price;
    }

}
