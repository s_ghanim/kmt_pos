/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

/**
 *
 * @author masiya
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BookingDataDetails implements Serializable {

    @JsonProperty("status_id")
    private String status_id;
    @JsonProperty("status_message")
    private String status_message;
    @JsonProperty("amount")
    private int amount;
    @JsonProperty("status")
    private String status;
    @JsonProperty("tick_pdf_url")

    private String tick_pdf_url;
    @JsonProperty("agent")
    private String agent;
    @JsonProperty("employee_id")
    private int employee_id;
    @JsonProperty("first_name")
    private String first_name;
    @JsonProperty("second_name")
    private String second_name;
    @JsonProperty("last_name")
    private String last_name;
    @JsonProperty("email")
    private String email;
    @JsonProperty("mobile")
    private String mobile;
    @JsonProperty("type")
    private String type;
    @JsonProperty("title")
    private String title;
    @JsonProperty("date")
    private String date;
    @JsonProperty("time")
    private String time;
    @JsonProperty("location_id")
    private String location_id;
    @JsonProperty("category_name")
    private String category_name;
    @JsonProperty("location_name")
    private String location_name;
    @JsonProperty("location_color")
    private String location_color;
    @JsonProperty("package_name")
    private String package_name;
    @JsonProperty("cover")
    private String cover;
    @JsonProperty("tickets")
    private TicketType[] tickets;
    @JsonProperty("package_dates")
    private String[] package_dates;
    // @JsonProperty("id")
    //  private String fees;

    /**
     * @return the status_id
     */
    public String getStatus_id() {
        return status_id;
    }

    /**
     * @param status_id the status_id to set
     */
    public void setStatus_id(String status_id) {
        this.status_id = status_id;
    }

    /**
     * @return the status_message
     */
    public String getStatus_message() {
        return status_message;
    }

    /**
     * @param status_message the status_message to set
     */
    public void setStatus_message(String status_message) {
        this.status_message = status_message;
    }

    /**
     * @return the amount
     */
    public int getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(int amount) {
        this.amount = amount;
    }

    /**
     * @return the agent
     */
    public String getAgent() {
        return agent;
    }

    /**
     * @param agent the agent to set
     */
    public void setAgent(String agent) {
        this.agent = agent;
    }

    /**
     * @return the employee_id
     */
    public int getEmployee_id() {
        return employee_id;
    }

    /**
     * @param employee_id the employee_id to set
     */
    public void setEmployee_id(int employee_id) {
        this.employee_id = employee_id;
    }

    /**
     * @return the first_name
     */
    public String getFirst_name() {
        return first_name;
    }

    /**
     * @param first_name the first_name to set
     */
    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    /**
     * @return the second_name
     */
    public String getSecond_name() {
        return second_name;
    }

    /**
     * @param second_name the second_name to set
     */
    public void setSecond_name(String second_name) {
        this.second_name = second_name;
    }

    /**
     * @return the last_name
     */
    public String getLast_name() {
        return last_name;
    }

    /**
     * @param last_name the last_name to set
     */
    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @param mobile the mobile to set
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return the time
     */
    public String getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * @return the category_name
     */
    public String getCategory_name() {
        return category_name;
    }

    /**
     * @param category_name the category_name to set
     */
    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    /**
     * @return the location_name
     */
    public String getLocation_name() {
        return location_name;
    }

    /**
     * @param location_name the location_name to set
     */
    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    /**
     * @return the location_color
     */
    public String getLocation_color() {
        return location_color;
    }

    /**
     * @param location_color the location_color to set
     */
    public void setLocation_color(String location_color) {
        this.location_color = location_color;
    }

    /**
     * @return the package_name
     */
    public String getPackage_name() {
        return package_name;
    }

    /**
     * @param package_name the package_name to set
     */
    public void setPackage_name(String package_name) {
        this.package_name = package_name;
    }

    /**
     * @return the cover
     */
    public String getCover() {
        return cover;
    }

    /**
     * @param cover the cover to set
     */
    public void setCover(String cover) {
        this.cover = cover;
    }

    /**
     * @return the tickets
     */
    public TicketType[] getTickets() {
        return tickets;
    }

    /**
     * @param tickets the tickets to set
     */
    public void setTickets(TicketType[] tickets) {
        this.tickets = tickets;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the tick_pdf_url
     */
    public String getTick_pdf_url() {
        return tick_pdf_url;
    }

    /**
     * @param tick_pdf_url the tick_pdf_url to set
     */
    public void setTick_pdf_url(String tick_pdf_url) {
        this.tick_pdf_url = tick_pdf_url;
    }

    /**
     * @return the location_id
     */
    public String getLocation_id() {
        return location_id;
    }

    /**
     * @param location_id the location_id to set
     */
    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    /**
     * @return the package_dates
     */
    public String[] getPackage_dates() {
        return package_dates;
    }

    /**
     * @param package_dates the package_dates to set
     */
    public void setPackage_dates(String[] package_dates) {
        this.package_dates = package_dates;
    }

    /**
     * @return the fees
     */
    /*  public String getFees() {
     return fees;
     }

   
     public void setFees(String fees) {
     this.fees = fees;
     }*/
}
