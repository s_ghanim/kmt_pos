/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

/**
 *
 * @author masiya
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Ticket implements Serializable {

    @JsonProperty("data")
    private TicketDataDetails data;
    @JsonProperty("error")
    private ErrorKmt error;

    /**
     * @return the data
     */
    public TicketDataDetails getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(TicketDataDetails data) {
        this.data = data;
    }

    /**
     * @return the error
     */
    public ErrorKmt getError() {
        return error;
    }

    /**
     * @param error the error to set
     */
    public void setError(ErrorKmt error) {
        this.error = error;
    }

}
