/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author masiya
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CancelOrder {

    @JsonProperty("error")
    private ErrorKmt error;

    @JsonProperty("data")
    private CancelOrderDetails data;

    /**
     * @return the error
     */
    public ErrorKmt getError() {
        return error;
    }

    /**
     * @param error the error to set
     */
    public void setError(ErrorKmt error) {
        this.error = error;
    }

    /**
     * @return the data
     */
    public CancelOrderDetails getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(CancelOrderDetails data) {
        this.data = data;
    }
}
