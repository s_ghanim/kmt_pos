/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author
 */
public class LocalProperties {

    final static String PROPERTIES_FILENAME = "/Users/masiya/application.properties";

    public String getString(String keyName) {
        try {
            Properties prop = new Properties();
            File initialFile = new File(PROPERTIES_FILENAME);
            InputStream targetStream = new FileInputStream(initialFile);
            // InputStream in = getClass().getResourceAsStream(PROPERTIES_FILENAME);
            prop.load(targetStream);
            targetStream.close();

            String translatedKeyName = prop.getProperty(keyName);
            if (translatedKeyName != null) {
                translatedKeyName = translatedKeyName.trim();
                if (!translatedKeyName.isEmpty()) {
                    return (translatedKeyName);
                } else {
                    return (keyName);
                }
            } else {
                return (keyName);
            }
        } catch (IOException ex) {
            return (keyName);
        }
    }

    public String getStringNoDefault(String keyName) {
        try {
            Properties prop = new Properties();
            InputStream in = getClass().getResourceAsStream(PROPERTIES_FILENAME);
            prop.load(in);
            in.close();

            String translatedKeyName = prop.getProperty(keyName);
            if (translatedKeyName != null) {
                translatedKeyName = translatedKeyName.trim();
                if (!translatedKeyName.isEmpty()) {
                    return (translatedKeyName);
                } else {
                    return ("");
                }
            } else {
                return ("");
            }
        } catch (IOException ex) {
            return ("");
        }
    }

    public double getDouble(String keyName) {
        return Double.parseDouble(this.getString(keyName));
    }

    public int getInteger(String keyName) {
        return Integer.parseInt(this.getString(keyName));
    }

    public boolean getBoolean(String keyName) {
        return Boolean.parseBoolean(this.getString(keyName));
    }

    public static String getString2(String keyName) {
        FileInputStream in = null;
        try {
            // create and load default properties
            Properties defaultProps = new Properties();
            in = new FileInputStream(PROPERTIES_FILENAME);
            //InputStream in = getClass().getResourceAsStream(PROPERTIES_FILENAME);
            defaultProps.load(in);

            return (defaultProps.getProperty(keyName));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(LocalProperties.class.getName()).log(Level.SEVERE, "LocalProperties:FileNotFoundException" + ex);
            return ("");
        } catch (IOException ex) {
            Logger.getLogger(LocalProperties.class.getName()).log(Level.SEVERE, "LocalProperties:IOException" + ex);
            return ("");
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
                Logger.getLogger(LocalProperties.class.getName()).log(Level.SEVERE, "LocalProperties:IOException" + ex);
            }
        }
    }
}
