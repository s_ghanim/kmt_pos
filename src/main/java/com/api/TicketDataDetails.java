/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

/**
 *
 * @author masiya
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TicketDataDetails implements Serializable {

    @JsonProperty("status")
    private int status;
    @JsonProperty("url")
    private String url;
    @JsonProperty("order_id")
    private String order_id;
    @JsonProperty("message")
    private String message;

    /**
     * @return the status_id
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status_id the status_id to set
     */
    public void setStatus_id(int status) {
        this.status = status;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the book_id
     */
    public String getOrder_id() {
        return order_id;
    }

    /**
     * @param book_id the book_id to set
     */
    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
