/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

/**
 *
 * @author masiya
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Details implements Serializable {

    @JsonProperty("age_group_name")
    private String age_group_name;

    @JsonProperty("quantity")
    private int quantity;

    /**
     * @return the age_group_name
     */
    public String getAge_group_name() {
        return age_group_name;
    }

    /**
     * @param age_group_name the age_group_name to set
     */
    public void setAge_group_name(String age_group_name) {
        this.age_group_name = age_group_name;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
