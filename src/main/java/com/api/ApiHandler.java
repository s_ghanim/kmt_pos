/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import org.json.JSONObject;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.net.ssl.HttpsURLConnection;
import org.apache.http.NameValuePair;

/**
 *
 * @author masiya
 */
public class ApiHandler {

    private String apiPath = "https://www.kmt.kw/dev/wp-json/mobile/v2";  //"https://7d107602-a627-4fde-863e-ca429570519f.mock.pstmn.io/";   ////"https://www.kmt.kw/dev/wp-json/mobile/v1"; //"https://7d107602-a627-4fde-863e-ca429570519f.mock.pstmn.io/";
    private String apiName = "";

    /**
     * @return the apiPath
     */
    public String getApiPath() {

        return apiPath;
    }

    /**
     * @param apiPath the apiPath to set
     */
    public void setApiPath(String apiPath) {
        this.apiPath = apiPath;
    }

    /**
     * @return the apiName
     */
    public String getApiName() {
        return apiName;
    }

    /**
     * @param apiName the apiName to set
     */
    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    private String getData(HttpsURLConnection conn) throws IOException {
        JSONObject jsonResponse = null;
        StringBuffer response = null;
        String readLine = null;
        conn.connect();
        int responseCode = conn.getResponseCode();
        if (responseCode == HttpsURLConnection.HTTP_OK) {
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            response = new StringBuffer();
            while ((readLine = in.readLine()) != null) {
                response.append(readLine);
            }
            in.close();

        } else {
            System.out.println("GET NOT WORKED");
        }

        return response.toString();
    }

    public String callAPI(Properties params, String path, String APIName) throws IOException, InstantiationException, IllegalAccessException {

        URL url = new URL(apiPath + path);
        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

        conn.setDoInput(true);
        conn.setDoOutput(true);

        switch (APIName) {
            case "login": {
                System.out.println("login");
                conn.setRequestMethod("POST");
                List<NameValuePair> params2 = new ArrayList<NameValuePair>();
                params2.add(new BasicNameValuePair("email", params.getProperty("email")));
                params2.add(new BasicNameValuePair("password", params.getProperty("password")));

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getQuery(params2));
                writer.flush();
                writer.close();
                os.close();

            }
            break;
            case "events": {
                conn.setRequestProperty("lang", "en");
            }
            break;
            case "eventDetails": {
                System.out.println("event details");
                conn.setRequestProperty("lang", "en");

            }
            break;
            case "TicketType": {
                conn.setRequestProperty("lang", "en");
            }
            break;
            case "TicketDetails": {

                conn.setRequestMethod("POST");
                List<NameValuePair> params2 = new ArrayList<NameValuePair>();
                conn.setRequestProperty("Authorization", "Bearer  " + params.getProperty("token"));
                //params2.add(new BasicNameValuePair("Authorization", "Bearer  "+params.getProperty("token")));
                params2.add(new BasicNameValuePair("first_name", params.getProperty("first_name")));

                params2.add(new BasicNameValuePair("last_name", params.getProperty("last_name")));
                params2.add(new BasicNameValuePair("mobile", params.getProperty("mobile")));
                params2.add(new BasicNameValuePair("email", params.getProperty("email")));
                params2.add(new BasicNameValuePair("selected", params.getProperty("selected")));
                params2.add(new BasicNameValuePair("event_id", params.getProperty("event_id")));
                params2.add(new BasicNameValuePair("payment_method", "cod"));
                params2.add(new BasicNameValuePair("payment_type", params.getProperty("payment_type")));
                params2.add(new BasicNameValuePair("employee_id",params.getProperty("employee_id")));
                
                params2.add(new BasicNameValuePair("agent", "pos"));
                params2.add(new BasicNameValuePair("lang", "en"));

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getQuery(params2));
                writer.flush();
                writer.close();
                os.close();
            }
            break;

            case "CancelOrder": {
                conn.setRequestProperty("username", params.getProperty("username"));
                conn.setRequestProperty("password", params.getProperty("password"));
                conn.setRequestProperty("book_id", params.getProperty("book_id"));

            }
            break;
            default: {
                System.out.println("default");
            }
        }
        // set userId its a sample here
        return getData(conn);

    }

    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params) {
            if (first) {
                first = false;
            } else {
                result.append("&");
            }

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }

}
