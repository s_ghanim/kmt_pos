/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

/**
 *
 * @author masiya
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PrintDetails implements Serializable {

    @JsonProperty("qrcode_url")
    private String qrcode_url;
    @JsonProperty("purchased_ticket_id")
    private String purchased_ticket_id;
    @JsonProperty("validity_date")
    private String validity_date;
    @JsonProperty("price")
    private String price;
    @JsonProperty("name")
    private String name;
    @JsonProperty("notes")
    private String notes;
    @JsonProperty("noOfDays")
    private String noOfDays;

    /**
     * @return the qrcode_url
     */
    public String getQrcode_url() {
        return qrcode_url;
    }

    /**
     * @param qrcode_url the qrcode_url to set
     */
    public void setQrcode_url(String qrcode_url) {
        this.qrcode_url = qrcode_url;
    }

    /**
     * @return the purchased_ticket_id
     */
    public String getPurchased_ticket_id() {
        return purchased_ticket_id;
    }

    /**
     * @param purchased_ticket_id the purchased_ticket_id to set
     */
    public void setPurchased_ticket_id(String purchased_ticket_id) {
        this.purchased_ticket_id = purchased_ticket_id;
    }

    /**
     * @return the price
     */
    public String getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @return the validity_date
     */
    public String getValidity_date() {
        return validity_date;
    }

    /**
     * @param validity_date the validity_date to set
     */
    public void setValidity_date(String validity_date) {
        this.validity_date = validity_date;
    }

    /**
     * @return the noOfDays
     */
    public String getNoOfDays() {
        return noOfDays;
    }

    /**
     * @param noOfDays the noOfDays to set
     */
    public void setNoOfDays(String noOfDays) {
        this.noOfDays = noOfDays;
    }
}
