/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

/**
 *
 * @author masiya
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Packages implements Serializable {

    @JsonProperty("package_id")
    private int package_id;
    @JsonProperty("package_name")
    private String package_name;
    @JsonProperty("package_dates")
    private String[] package_dates;
    @JsonProperty("ticket_types")
    private TicketType[] ticket_types;

    /**
     * @return the package_name
     */
    public String getPackage_name() {
        return package_name;
    }

    /**
     * @param package_name the package_name to set
     */
    public void setPackage_name(String package_name) {
        this.package_name = package_name;
    }

    /**
     * @return the ticket_types
     */
    public TicketType[] getTicket_types() {
        return ticket_types;
    }

    /**
     * @param ticket_types the ticket_types to set
     */
    public void setTicket_types(TicketType[] ticket_types) {
        this.ticket_types = ticket_types;
    }

    /**
     * @return the package_id
     */
    public int getPackage_id() {
        return package_id;
    }

    /**
     * @param package_id the package_id to set
     */
    public void setPackage_id(int package_id) {
        this.package_id = package_id;
    }

    /**
     * @return the package_dates
     */
    public String[] getPackage_dates() {
        return package_dates;
    }

    /**
     * @param package_dates the package_dates to set
     */
    public void setPackage_dates(String[] package_dates) {
        this.package_dates = package_dates;
    }

}
