

<%@page import="java.awt.image.BufferedImage"%>
<%@page import="java.io.File"%>
<%@page import="java.util.Properties"%>
<%@page import="javax.imageio.ImageIO"%>
<%@page import="java.net.URL"%>
<%@page import="java.awt.Image"%>
<%@page import="com.api.TicketPrintedData"%>
<%@page import="java.util.ArrayList"%>
<html lang="en">
    <head>

        <title>KMT Point Of Sale</title>

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,500,600,700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" >
        <link rel="stylesheet" href="css\main.css">
    </head>




    <body class="stretched">
        <header>
            <nav class="navbar navbar-expand-lg navbar-dark bg-danger  pl-3">
                <a class="navbar-brand" href="#"><img src="img/logo.png" class="img-fluid" alt=""></a>
                <h3><font color='white'>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Development</font></h3>
           
 <form action="${pageContext.request.contextPath}/logout" method="post">

     <input type="submit" value="Logout"  class="btn btn-dark mt-3 skew text-left" onclick="return resetData();" style="position: absolute;right:5;top:0;height:30px;width:70px;font-size: 12px;" />

            </form>
                
                
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar"  aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>


            </nav>
        </header>
        <div id="wrapper" class="clearfix">
            <section id="content" style="background-color: #E7E7E7;">
                <div class="content-wrap nopadding">
                    <div class="section nopadding nomargin"></div>
                    <div class="section nobg full-screen nopadding nomargin">
                        <div class="container-fluid vertical-middle divcenter clearfix">

                            <div class="card divcenter noborder" style="max-width: 400px;background-color: transparent;">
                                <div class="card-body">
                                    <div class="alert-section"></div>
                                    <form   method="post" action="start.jsp" >
                                        <%
                                            String errMsg = (String) request.getParameter("errMessage");
                                            if (errMsg == null) {
                                                errMsg = "";
                                            }
                                        %>
                                        <div >

                                            <h4 class="text-dark mb-3">Login Details</h4>
                                            <label for="email">Username:</label>
                                            <input type="text" id="email" name="email" class="form-control bg-white border-secondary br-30 mb-2" placeholder="Username or email address">
                                            <label for="password">Password:</label>
                                            <input type="Password" id="password" name="password" class="form-control bg-white border-secondary br-30 mb-2" placeholder="Password">

                                            <button id="loginButton" name="loginButton" class="btn btn-dark skew px-4 mt-2">
                                                <span>Login</span>
                                            </button>
                                            
                                            
                                            <p class="lead text-danger font-weight-bold mt-2">
                                                <input class="lead text-danger font-weight-bold mt-2" type="text" id="errMsg" name="errMsg" value="<%=errMsg%>" style="border:none;background-color: #E7E7E7;" disabled>
                                            </p>

                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




            </section><!-- #content end -->
        </div>
        <!-- Go To Top 	============================================= -->
        <div id="gotoTop" class="icon-angle-up"></div>

        <script src="js/jquery.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/functions.js"></script>
        <script src="js/sweet-alert.min.js"></script>
        <script src="js/new-js.js"></script>
        <script>
             function resetData()
                            {

                               
                                document.getElementById('email').value = "";
                                 document.getElementById('password').value = "";

                                return true;
                               
                            }
            </script>
      
    </body>
</html>