
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.api.UserInfo"%>
<%@page import="com.api.BookingData"%>
<%@page import="com.api.LoginDetails"%>
<%@page import="com.api.ErrorKmt"%>
<%@page import="com.api.Login"%>
<%@page import="com.api.EventDetails"%>
<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%>
<%@page import="com.fasterxml.jackson.core.type.TypeReference"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.api.Events"%>
<%@page import="com.api.ApiHandler"%>
<%@page import="java.util.Properties"%>
<%@page import="org.json.JSONObject" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Date" %>
<head>
    <title>KMT Point Of Sale</title>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" >
    <link rel="stylesheet" href="css\main.css">
    <style>
   #loading-image {
  position: absolute;
  top: 100px;
  left: 240px;
  z-index: 100;
}
            .grayout {
                opacity: 0.6; 
                filter: alpha(opacity = 60); 
            } 
          #load{
    width:100%;
    height:100%;
    position:fixed;
    z-index:9999;
    background-color: #fff;
    background:"img/kmt_loading.gif"
}
</style>
</head>

<body class="text-dark" style="height:100%;">
     <div id="load">
        
            <img id="loading-image" src="img/kmt_loading.gif" alt="Loading..." />
    </div>
       
   <div id="pageContent">
    <header>
        <nav class="navbar navbar-expand-lg navbar-dark bg-danger  pl-3">
            
            <a class="navbar-brand" href="#"><img src="img/logo.png" class="img-fluid" alt=""></a>
            <h3><font color='white'>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Development</font></h3>
            <form action="logout.jsp" method="post">

                <input type="submit" value="Logout"  class="btn btn-dark mt-3 skew text-left" style="position: absolute;right:5;top:0;height:30px;width:70px;font-size: 12px;" />

            </form>
            
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar"  aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>


        </nav>
    </header>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4  px-0 d-flex flex-column" style="padding-top: 0rem">
                    <div class="pr-lg-4 pt-4 order-1 order-lg-1 pr-md-2  mx-lg-4 px-2">
                        <h3 class="text-uppercase">AVAILABLE EVENTS</h3>
                        <ul class="nav nav-tabs flex-column">


                            <%

                                int employee_id = -1;
                                try {
                                    Properties params = new Properties();
                                    ApiHandler handler = new ApiHandler();
                                    String token = null;
                                    String jsonLoginStr = null;
                                    params.put("email", (String) request.getParameter("email"));
                                    params.put("password", (String) request.getParameter("password"));
                                    jsonLoginStr = handler.callAPI(params, "/login", "login");
                                    ObjectMapper detailMapper = new ObjectMapper();
                                    TypeReference<Login> detailType = new TypeReference<Login>() {
                                    };
                                    String bookId = "";
                                    Login jsonDetail = detailMapper.readValue(jsonLoginStr, detailType);
                                    ErrorKmt error = jsonDetail.getError();
                                    if (error != null) {
                                        int errorNo = error.getCode();
                                        String errMessage = error.getMessage();
                                        response.sendRedirect("index.jsp?errMessage=" + errMessage);

                                    } else {
                                        LoginDetails data = jsonDetail.getData();
                                        employee_id = data.getUser().getUser_id();
                                        token = data.getUser().getToken();
                                        String user_rule = data.getUser().getRole();
                                        if (!user_rule.equals("pos") && !user_rule.equals("Manager")) {

                                            String errMessage = "you are not allowed to acess POS";
                                            response.sendRedirect("index.jsp?errMessage=" + errMessage);

                                        } else {
                                            session.setAttribute("user", request.getParameter("email"));
                                            params = new Properties();
                                            String jsonEventsStr = null;
                                            params.put("lang", "en");
                                            handler = new ApiHandler();
                                            jsonEventsStr = handler.callAPI(params, "/events", "events");

                                            String newjsonEventsStr = jsonEventsStr.substring(jsonEventsStr.indexOf("["), jsonEventsStr.indexOf("]") + 1);

                                            TypeReference<ArrayList<Events>> mapType = new TypeReference<ArrayList<Events>>() {
                                            };
                                            ObjectMapper objectMapper = new ObjectMapper();
                                            ArrayList<Events> jsonToList = objectMapper.readValue(newjsonEventsStr, mapType);

                                            //SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                                            // Date today = new Date();
                                            // String ticketTime = formatter.format(date); 
                                            //2019-09-11
                                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                                            Date today = sdf.parse(sdf.format(new Date()));
                                            Date startDate = sdf.parse(sdf.format(new Date()));
                                            Date endDate = sdf.parse(sdf.format(new Date()));

                                            for (int i = 0; i < jsonToList.size(); i++) {
                                                String eventDate = jsonToList.get(i).getDate();
                                                if (eventDate.length() > 0) {
                                                    String[] arrSplit = eventDate.split("\\s-\\s");

                                                    if (arrSplit.length == 2) {
                                                        startDate = sdf.parse(arrSplit[0].trim());
                                                        endDate = sdf.parse(arrSplit[1].trim());
                                                    }

                                                    if (arrSplit.length == 1) {
                                                        endDate = sdf.parse(arrSplit[0].trim());
                                                    }
                                                   /* if ((today.compareTo(endDate) > 0)) {
                                                        jsonToList.remove(i);
                                                        i--;
                                                    }*/
                                                }

                                            }
//}      

                                            if (jsonToList.size() == 0) {
                                                response.sendRedirect("errorHome.jsp?errMessage=No Available Events");
                                            }
                                            request.setAttribute("allEvents", jsonToList);

                                            request.setAttribute("employee_id", employee_id);
                                            request.setAttribute("token", token);
                                        }
                                    }
                                } catch (Exception e) {
                                    String errorMsg = "Internal error please contact system adminstrator";

                                    response.sendRedirect("errorHome.jsp?errMessage=" + errorMsg + "&errorDetails=" + e.toString());
                                }
                                // }
                            %>


                            <c:forEach items="${allEvents}" var="event">
                                <li class="nav-item">
                                    <a id="chosenevent"    class="nav-link text-dark p-0 m-0 my-2 chos" data-toggle="tab"   target="myIframe" href="ticketForm.jsp?eventId=${event.id}&eventTitle=${event.title}&employee_id=${employee_id}&remaining=${event.remaining}&token=${token}&isavailable=${event.is_available}"  >
                                        <div class="event font-weight-bold shadow fz-14" >
                                            <div class="head bg-white pr-md-5 px-3 py-2 text-uppercase">
                                                <p class=" m-0">${event.title}</p>
                                            </div>
                                            <div class="info bg-light px-3 py-2 text-danger d-flex justify-content-between">
                                                <p class="m-0">${event.date} </p>

                                            </div>
                                        </div>
                                    </a>
                                </li>

                            </c:forEach>

                        </ul>
                    </div>

                </div>
                <input type="hidden" id="employee_id" name="employee_id" value="">
                <div class="col-lg-8  px-0 d-flex flex-column" >
                    <iframe name="myIframe"   height="600" width="900" frameborder="0" style="border-bottom-color:white">
                        
                    </iframe>
                </div>


            </div>
        </div>
    </section>


   </div>

    <script src="js/jquery.js"></script>

    <script src="js/functions.js"></script>

    <script>
       
      
          function resetData()
                            {

                               
                                document.getElementById('email').value = "";
                                 document.getElementById('password').value = "";

                                return true;
                               
                            }

        $('.option').on('click', function (event) {
            event.preventDefault();
            /* Act on the event */
            $(this).siblings('input').trigger('click');
        });


        $('input[type=radio]').on('click', function () {
            $(this).parent('li').siblings('li').children('a.btn').removeClass('active');
            $(this).siblings('a.btn').addClass('active');
        });

        function showcaseSection(element) {
            var otherElements = element.parents('.showcase-section').find('.showcase-feature'),
                    elementTarget = jQuery(element.attr('data-target')),
                    otherTargets = element.parents('.showcase-section').find('.showcase-target');

            otherElements.removeClass('showcase-feature-active');
            element.addClass('showcase-feature-active');
            otherTargets.removeClass('showcase-target-active');
            elementTarget.addClass('showcase-target-active');

        }

        jQuery('.showcase-feature').hover(function () {
            showcaseSection(jQuery(this));
        });

        var employee_id;
        $("#chosenevent").click(function () {

            document.getElementById('employee_id').value = <%=employee_id%>;
            employee_id =<%=employee_id%>;
        });


        $('.chos').on('click', function () {



            $(this).parent('li').siblings('li').children('a.chos').removeClass('active');
            $(this).addClass('active');
        });

 document.onreadystatechange = function () {
    
  var state = document.readyState
  if (state == 'interactive') {
       document.getElementById('pageContent').style.visibility="hidden";
  } else if (state == 'complete') {
      setTimeout(function(){
         document.getElementById('interactive');
         document.getElementById('load').style.visibility="hidden";
         document.getElementById('pageContent').style.visibility="visible";
      },1000);
  }
}
  

    </script>
</body>
</html>
