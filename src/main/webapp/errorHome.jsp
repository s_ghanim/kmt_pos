

<html lang="en">
    <head>

        <title>KMT Point Of Sale</title>
        <link rel="icon" href="am.png">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,500,600,700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" >
        <link rel="stylesheet" href="css\main.css">
    </head>




    <body class="stretched">
        <header>
            <nav class="navbar navbar-expand-lg navbar-dark bg-danger  pl-3">
                <a class="navbar-brand" href="#"><img src="img/logo.png" class="img-fluid" alt=""></a>
                 <h3><font color='white'>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Development</font></h3>
           
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar"  aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>


            </nav>
        </header>
        <div id="wrapper" class="clearfix">
            <section id="content" style="background-color: #E7E7E7;">
                <div class="content-wrap nopadding">
                    <div class="section nopadding nomargin"></div>
                    <div class="section nobg full-screen nopadding nomargin">
                        <div class="container-fluid vertical-middle divcenter clearfix">

                            <div class="card divcenter noborder" style="max-width: 400px;background-color: transparent;">
                                <div class="card-body">
                                    <div class="alert-section"></div>
                                    <form action="index.jsp" method="post" >
                                    
        <%
            String errorMessage = (String)request.getParameter("errMessage");
            String errorNo = (String)request.getParameter("errorNo");
            String errorDetails = (String)request.getParameter("errorDetails");
            request.setAttribute("errorNo", errorNo);
            request.setAttribute("errorMessage", errorMessage);
            request.setAttribute("errorDetails", errorDetails);
            
            %>
        
           
          <p class="lead font-weight-bold mt-2">
               <span class="lead font-weight-bold mt-2" style="color:navy ">Error : </span>
             <input class="lead text-danger font-weight-bold mt-2" type="text"  size="100" id="errorMessage" name="errorMessage" value="${errorMessage}" style="border:none;background-color: #E7E7E7;" disabled>
             <input class="lead  font-weight mt-2" type="text"  size="100" id="errorDetails" name="errorDetails" value="${errorDetails}" style="border:none;background-color: #E7E7E7;" disabled>
            
             </p>
             
              <button type="submit" id="loginButton" name="errorButton" class="btn btn-dark skew px-4 mt-2">



                                                <span>Back to Home</span></button>
                                           
           



                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>




            </section><!-- #content end -->
        </div>
        <!-- Go To Top 	============================================= -->
        <div id="gotoTop" class="icon-angle-up"></div>
  <script>
              
               $(document).ready(function(){
   
      alert("Image successfully loaded.");
   document.getElementById("errorMessage").value="<%=errorMessage%>";
});

                
             </script>  
  
        <script src="js/jquery.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/functions.js"></script>
        <script src="js/sweet-alert.min.js"></script>
        <script src="js/new-js.js"></script>
       
    </body>
</html>
   

