<%-- 
    Document   : ticketForm
    Created on : Sep 8, 2019, 11:49:19 AM
    Author     : masiya
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.api.EventDetails"%>
<%@page import="com.api.BookingData"%>
<%@page import="com.api.ErrorKmt"%>
<%@page import="com.api.Ticket"%>
<%@page import="java.util.Properties"%>
<%@page import="com.api.ApiHandler"%>
<%@page import="com.fasterxml.jackson.core.type.TypeReference"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%>
<%@page import="com.api.Packages"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html lang="en"  style="background-color: white">
    <head>

        <title>KMT Point Of Sale</title>

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,500,600,700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" >
        <link rel="stylesheet" href="css\main.css">
        <link rel="stylesheet" href="css\additional.css">
        <style>
          /* #loadingDiv {
   width: 100%;
   height: 100%;
   top: 0;
   left: 0;
   position: fixed;
   display: block;
   opacity: 0.7;
   background-color: #fff;
   z-index: 99;
   text-align: center;
}
*/
#loading-image {
  position: absolute;
  top: 100px;
  left: 240px;
  z-index: 100;
}
            .grayout {
                opacity: 0.6; 
                filter: alpha(opacity = 60); 
            } 
          #load{
    width:100%;
    height:100%;
    position:fixed;
    z-index:9999;
    background-color: #fff;
    background:"img/kmt_loading.gif"
}
        </style>
    </head>
    
    <body>
        
        <div id="load">
        
            <img id="loading-image" src="img/kmt_loading.gif" alt="Loading..." />
    </div>
       
   <div id="pageContent">
         
   
       
 
        <%

            if (session.getAttribute("user") == null) {
                String errMessage = "session is expired.";
                response.setHeader("Pragma", "no-cache");
                response.setHeader("Cache-Control", "no-store");
                response.setHeader("Expires", "0");
                response.setDateHeader("Expires", -1);
                session.setAttribute("user", null);
                session.invalidate();

                RequestDispatcher rd = request.getRequestDispatcher("relogin.jsp?errMessage=" + errMessage);
                rd.forward(request, response);
                //response.reset();
                // request.logout();
                // response.sendRedirect("relogin.jsp?errMessage=" + errMessage);
    /* String see =  request.getContextPath();
                 String See2=request.getPathInfo();
                 String Seer3=request.getRequestURI();
                 String Seer4=request.getServletPath();
    
                 //response.sendRedirect("index.jsp?errMessage=" + errMessage);
                 RequestDispatcher rd = request.getRequestDispatcher("relogin.jsp?errMessage=" + errMessage);
                 rd.forward(request, response);*/

            }

            String errMessage = null;
            String PackagesStr = "";
            String employee_id = (String) request.getParameter("employee_id");
            String eventId = (String) request.getParameter("eventId");
            String isavailable = (String) request.getParameter("isavailable");
            String eventTitle = (String) request.getParameter("eventTitle");
            String eventremaining = (String) request.getParameter("remaining");
            String logintoken = (String) request.getParameter("token");

            if (isavailable.equals("false")) {
                response.sendRedirect("blank.jsp");
            } else {

                try {

                    request.setAttribute("eventId", eventId);
                    request.setAttribute("eventTitle", eventTitle);
                    request.setAttribute("employee_id", employee_id);

                    String jsonTicketTypesStr = null;
                    Properties params = new Properties();

                    params.put("lang", "en");
                    ApiHandler handler = new ApiHandler();

                    jsonTicketTypesStr = handler.callAPI(params, "/form/event/" + eventId +"?lang=en", "TicketType");

                    int errorExist = 0;
                    errorExist = jsonTicketTypesStr.indexOf("error");
                    if (errorExist > 0) {
                        String errorNo = jsonTicketTypesStr.substring(jsonTicketTypesStr.indexOf("code"), jsonTicketTypesStr.indexOf("message"));
                        errMessage = jsonTicketTypesStr.substring(jsonTicketTypesStr.indexOf("message"), jsonTicketTypesStr.indexOf("}"));
                        response.sendError(1, errMessage);
                        response.sendRedirect("errorAll.jsp?errMessage=" + errMessage + "&errorNo=" + errorNo);
                    } else {

                        String newjsonTicketType = jsonTicketTypesStr.substring(jsonTicketTypesStr.indexOf("packages"));
                        String newjsonTicketType2 = newjsonTicketType.substring(newjsonTicketType.indexOf("["), newjsonTicketType.length() - 2);
                        PackagesStr = newjsonTicketType2.replace("\"", "\\\"");
                        TypeReference<ArrayList<Packages>> mapType2 = new TypeReference<ArrayList<Packages>>() {
                        };
                        ObjectMapper objectMapper2 = new ObjectMapper();
                        ArrayList<Packages> jsonToList2 = objectMapper2.readValue(newjsonTicketType2, mapType2);

                        request.setAttribute("allPackages", jsonToList2);
                    }

                } catch (Exception e) {
                    errMessage = e.getMessage();
                    response.sendRedirect("errorAll.jsp?errMessage=" + errMessage + "&errorNo=" + 0);
                }

            }
        %>
        <form action="result.jsp" class="form-group" style="background-color:white;padding-left: 20px;padding-top: 5px;" >
            <input type="hidden" id="eventId" name="eventId" value="">
            <input type="hidden" id="ticketTypes" name="ticketTypes" value="">
            <input type="hidden" id="employee_id" name="employee_id" value="">
            <input type="hidden" id="pay_method" name="pay_method" value="">
            <input type="hidden" id="token" name="token" value="">
            <input type="hidden" id="totalQty" name="totalQty" value="">
            <input type="hidden" id="totalPrc" name="totalPrc" value="">
            <input type="hidden" id="detail" name="detail" value="">
            <input type="hidden" id="total" name="total" value="">



            <p class="lead text-danger font-weight-bold ">
                <input class="lead text-danger font-weight-bold " type="text" size="100" id="errorMessage" name="errorMessage" value="" style="border:none;background-color: white;" disabled>

            </p> 


            <div>
                <div class="list-inline-item" >
                    <a href='#' class="btn btn-pos-kmt skew font-weight-bold px-4 option TickType1" id="vipParent"><span class="px-3">vip</span></a>
                    <input type="radio" name="ticket-type_vip" onclick="changeColorTickType1();" id="ticket-type_vip" class="sr-only">
                </div>
                <div  class="list-inline-item">
                    <a  href='#' class="btn btn-pos-kmt skew font-weight-bold px-4 option TickType2" id="normalParent"><span>General</span></a>
                    <input type="radio" name="ticket-type_normal" id="ticket-type_normal" onclick="changeColorTickType2();"  class="sr-only">
                </div>

                <div class="list-inline-item">
                </div>
                <div class="list-inline-item">
                    <label  class="small font-weight-bold">Package :</label>
                    <select class="custom-select br-30" name="avPackages" id="avPackages"  >

                        <option id="pkg" value="none">-- Select One --</option>
                        <c:forEach items="${allPackages}" var="package">
                            <option value="${package.package_id}"  name="${package.package_id}" >${package.package_name}   </option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <!-- <p class="lead text-danger font-weight-bold mt-2">
                 <input class="lead text-danger font-weight-bold mt-2" type="text" id="availableTickets" name="availableTickets" value="" style="border:none;background-color: white;" disabled>
             </p>-->
            <ul class="list-inline list-unstyled m-0 my-4" id="parent">





            </ul>

            <h4 class="pt-lg-4 font-weight-bold">CUSTOMER PROFILE</h4>
            <div class="row mt-2" >
                <div class="col-lg-7 col-md-12">
                    <div class="row mt-2"  >
                        <div  class="md-6  ">
                            <input type="text" class="form-control fz-14  br-30" id="form-phone" size="30" name="form-phone" value="" placeholder="Mobile">
                        </div>
                        <div  class="md-6  ">
                            <input type="email" class="form-control fz-14  br-30" id="formEmail" size="30" name="formEmail"  value="" placeholder="Email">
                        </div>
                    </div> 
                    <div class="row mt-2" >
                        <div  class="md-6  ">
                            <input type="text" class="form-control fz-14 mb-2 br-30" size="30" id="form-name" value="" name="form-name" placeholder="First Name">
                        </div>
                        <div class="md-6   mr-0">
                            <input type="text" class="form-control fz-14  br-30 " size="30" value="" id="last-name" name="last-name" placeholder="Last Name">
                        </div>
                    </div>
                </div>

            </div>
            <div class="row" >

                <div class="list-inline-item mt-2 mr-4" id="disp2" >
                    <h5 class=" m-0 text-uppercase font-weight-bold">Total</h5>
                    <h2 class="font-weight-bold"><input type="text"  class="font-weight-bold" id="totalPrice" name="totalPrice" value="" size="8" style="border:none;background-color: white;" disabled></h2>
                </div>



                <div>
                    <div class="list-inline-item mt-4" >
                        <a href='#' id="cashParent" class="btn btn-pos-kmt skew font-weight-bold px-4 option PayType1"><span class="px-3">Cash</span></a>
                        <input type="radio" name="payment-type-cash" id="payment-type-cash" class="sr-only" onclick="changeColorPayType1();">
                    </div>
                    <div  class="list-inline-item">
                        <a  href='#' id="knetParent" class="btn btn-pos-kmt skew font-weight-bold px-4 option PayType2"  style =""><span>Knet</span></a>
                        <input type="radio" name="payment-type-knet" id="payment-type-knet" onclick="changeColorPayType2();" class="sr-only">
                    </div>
                </div>  



                <div class="list-inline-item mt-2" >
                    <p class="m-0 ml-5">
                       <!-- <input type="checkbox" id="sendMail" name="sendMail" value="yes" hidden> send tickets by mail<br>-->
                        <button id="bookButton" class="btn btn-outline-danger text-capitalize fz-37 pb-1 pt-0 btn-lg font-weight-bold px-4 mt-4" data-toggle="modal" data-target="result.jsp"  onclick="return checkData();" type="submit" >Book</button>
                    </p>
                </div>


        </form>
 
        <!-- Modal -->

        <script src="js/jquery.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/functions.js"></script>
        <script>
            var selectedType = "";
                            $('.option').on('click', function (event) {
                                event.preventDefault();
                                /* Act on the event */
                                $(this).siblings('input').trigger('click');
                            });
                            $('input[type=radio]').on('click', function () {
                                $(this).parent('div').siblings('div').children('a.btn').removeClass('active');
                               
                                
                                $(this).siblings('a.btn').addClass('active');

                            });

                            $('#ticket-type_vip').on('click', function () {
                                document.getElementById("pkg").selected = 'none';
                                selectedType = "VIP";
                                document.getElementById("ticket-type_normal").checked =false;
                                //document.getElementById("vipParent").style ="box-shadow: inset 0 1px 1px black, 0 0 0 0.2rem black;"
                                //document.getElementById("normalParent").style ="";
                            });
                            $('#ticket-type_normal').on('click', function () {
                                document.getElementById("pkg").selected = 'none';
                                selectedType = "General Admission";
                                document.getElementById("ticket-type_vip").checked =false;
                                //document.getElementById("normalParent").style ="box-shadow: inset 0 0 1px gray, 0 0 0 0.2rem gray;"
                                //document.getElementById("vipParent").style ="";

                            });

                            $('#payment-type-knet').on('click', function () {

                                //document.getElementById("knetParent").style ="box-shadow: inset 0 0 1px gray, 0 0 0 0.2rem gray;"
                                // document.getElementById("cashParent").style ="";
                            });
                            $('#payment-type-cash').on('click', function () {

                                //document.getElementById("cashParent").style ="box-shadow: inset 0 0 1px gray, 0 0 0 0.2rem gray;"
                                // document.getElementById("knetParent").style ="";

                            });

                            var chosen = 0;



                            function showcaseSection(element) {
                                var otherElements = element.parents('.showcase-section').find('.showcase-feature'),
                                        elementTarget = jQuery(element.attr('data-target')),
                                        otherTargets = element.parents('.showcase-section').find('.showcase-target');

                                otherElements.removeClass('showcase-feature-active');
                                element.addClass('showcase-feature-active');
                                otherTargets.removeClass('showcase-target-active');
                                elementTarget.addClass('showcase-target-active');

                            }
                            jQuery('.showcase-feature').hover(function () {
                                showcaseSection(jQuery(this));
                            });


                            $("#payment-type").on('click', function () {

                                if (document.getElementById('payment-type').checked) {

                                    document.getElementById('payment_method').value = document.getElementById('payment-type').value;
                                    // alert("payment method chosen : checked" + document.getElementById('payment_method').value);
                                }
                            });
                            var token = "";
                            var payment_method = "";
                            var selectedPackage = -1;
                            //var selectedType = "General Admission";
                            $("#avPackages").on('change', function () {

                                if (document.getElementById("ticket-type_normal").checked == false && document.getElementById("ticket-type_vip").checked == false)
                                {
                                    // alert("please choose Category First (VIP - General admission)");
                                    document.getElementById('errorMessage').value = "Please choose Category :(VIP - General)";
                                    document.getElementById("pkg").selected = 'none';
                                } else
                                {

                                    if (document.getElementById("ticket-type_normal").checked == true)
                                    {
                                        selectedType = "General Admission";
                                    }


                                    else
                                    {
                                        selectedType = "VIP";
                                    }



                                    selectedPackage = $(this).val();
                                    
                                    if ((document.getElementById("avPackages").value != 'none'))
                                    {
                                           
                                        getTypes();


                                    }
                                }

                            });
                            var available_tickets = 0;
                            var packagesdata = 0;
                            var data;
                            var details;

                            function getTypes()
                            {
                                //alert("start of get types");

                                available_tickets = 0;
                                var s = "<%=PackagesStr%>";
                                var neededPart = "\"package_id\":" + selectedPackage;
                                 //alert("needed part" + neededPart);

                                var packageData = s.substring(s.indexOf((neededPart)));
                                //alert("package data for " + selectedPackage + " is " + packageData);
                                var isAvailable = packageData.substring((packageData.indexOf("is_available") + 14), packageData.indexOf("is_available") + 19);
                                //alert("is_available:" + isAvailable);

                                var newjsonTicketType = packageData.substring(packageData.indexOf("ticket_types"));

                                var jsTickets = newjsonTicketType.substring(newjsonTicketType.indexOf("["), newjsonTicketType.indexOf("]"));

                                var jsTickets2 = jsTickets + "]";
                                //alert("selected package data "+jsTickets2);
                                data = $.parseJSON(jsTickets2);
                                if (document.getElementById("tickets"))
                                {

                                    document.getElementById("parent").removeChild(document.getElementById("tickets"));
                                }
                                var ticketEle = document.createElement('li');
                                ticketEle.className = 'col-lg-8 col-md-8';
                                ticketEle.id = 'tickets';
                                ticketEle.innerHTML = '';
                                document.getElementById("parent").appendChild(ticketEle);

                                var tableEle = document.createElement('li');
                                tableEle.className = 'table paddingBetweenCols';
                                tableEle.id = 'table';
                                //alert ("isAvailable-" + isAvailable+"-");
                                if (isAvailable == "false")
                                {
                                    
                                    var tableStr = '<font color="red">This package disabled by the admin kindly choose another one</font><table class="grayout"><tr><th "padding:8px">category</th><th "padding:8px">KD</th>' +
                                            '<th padding:8px">Qty    </th></tr>';
                                } else
                                {
                                    
                                    var tableStr = '<table ><tr><th style="text-align:center;padding:0 30px;" >Category</th><th style="text-align:center;padding:0 30px;">KD</th>' +
                                            '<th style="text-align:center;padding:0 30px;">Qty</th></tr>';
                                }


                                for (var key in data) {
                                    //alert("key value is :" + key);
                                    var type_name = data[key].type_name;
                                    //alert("key value is :" + data[key].type_name);
                                    // alert('type_name :'+type_name );
                                    var age_group_name = data[key].age_group_name;
                                    //alert('age_group_name :'+age_group_name );
                                    var remaining = data[key].remaining;
                                    //alert('remaining :'+remaining );
                                    var price = data[key].price;
                                    data[key].quantity = 0;
                                    data[key].package_id = selectedPackage;


                                    if (Number(price) <= 0)
                                    {
                                        if (isAvailable == "false")
                                        {
                                            price = "<font >Free</font>";
                                        } else
                                        {
                                            price = "<font color='red'>Free</font>";
                                        }
                                    }
                                    if (Number(remaining) <= 0)
                                    {
                                        if (isAvailable == "false")
                                        {
                                            actualPrice = "<td style='text-align:center;padding:0 20px;padding-bottom: 1em;' ><font > SOLD OUT</font></td>" +
                                                    "<td style='text-align:center;padding:0 20px;padding-bottom: 1em;'><input id =qty_" + key + "   type='hidden' class='qty'> </td> ";
                                        } else
                                        {
                                            actualPrice = "<td style='text-align:center;padding:0 20px;padding-bottom: 1em;' ><font color='red'> SOLD OUT</font></td>" +
                                                    "<td style='text-align:center;padding:0 20px;padding-bottom: 1em;'><input id =qty_" + key + "   type='hidden' class='qty'> </td> ";

                                        }

                                    } else
                                    {
                                        if (isAvailable == "false")
                                        {
                                            actualPrice = "<td style='text-align:center;padding:0 20px;padding-bottom: 1em;'>" + price + "</td>" +
                                                    // '<td><input id =qty_'+key+'type="number" class="qty"   onchange= "calcSum(' + key + ',$(this).val())"  name ="' + key + '" id="qty_1" min="0"  max="100" value="0"> </td> '+ 
                                                    '<td  style="text-align:center;padding:0 20px;padding-bottom: 1em;"><select disabled class="custom-select fz-14 br-30 qty customDropdown "  id =qty_' + key + ' name=qty_' + key + '  type="number" onchange= "calcSum(' + key + ',$(this).val())"  style="width: 200px;background-color: white;color:black;" >' +
                                                    '<option  value=""></option>';
                                        } else
                                        {
                                            actualPrice = "<td style='text-align:center;padding:0 20px;padding-bottom: 1em;'>" + price + "</td>" +
                                                    // '<td><input id =qty_'+key+'type="number" class="qty"   onchange= "calcSum(' + key + ',$(this).val())"  name ="' + key + '" id="qty_1" min="0"  max="100" value="0"> </td> '+ 
                                                    '<td style="text-align:center;padding:0 20px;padding-bottom: 1em;"><select class="custom-select fz-14 br-30  qty customDropdown"  id =qty_' + key + ' name=qty_' + key + '  type="number" onchange= "calcSum(' + key + ',$(this).val())"  style="width: 200px;background-color: white;color:black;" >' +
                                                    '<option  value=""></option>';
                                        }
                                        for (ct = 1; ct <= remaining; ct++)
                                        {
                                            actualPrice = actualPrice + '<option  style="font-size:20px;" value="' + ct + '">' + ct + '</option>';
                                        }
                                        if (isAvailable == "false")
                                        {

                                            actualPrice = actualPrice + '</select></td><td style="text-align:center;padding:0 20px;padding-bottom: 1em;"><font >  ' + remaining + ' left</font></td>';
                                        } else
                                        {
                                            actualPrice = actualPrice + '</select></td><td style="text-align:center;padding:0 20px;padding-bottom: 1em;"><font color="red">  ' + remaining + ' left</font></td>';

                                        }
                                    }
                                    //alert(selectedType);
                                    //alert(actualPrice);
                                    if (type_name == selectedType)
                                    {
                                        //alert(available_tickets + "    "+remaining);
                                        available_tickets += remaining;
                                        // alert(available_tickets);
                                        tableStr = tableStr + '<tr ><td style="text-align:center;padding:0 20px;padding-bottom: 1em;">' + age_group_name + '</td>' + actualPrice + '</tr>';

                                    }
                                }
                                tableStr = tableStr + '</table>';
                                alert(tableStr);
                                tableEle.innerHTML = tableStr;
                                document.getElementById('tickets').appendChild(tableEle);




                                //  document.getElementById('availableTickets').value = available_tickets  +"   tickets available";
                                document.getElementById('employee_id').value = "<%=employee_id%>";
                                document.getElementById('eventId').value = "<%=eventId%>";

                            }

                            var sumqty = 0;
                            var sumprc = 0;
                            var selectedTickets = "";
                            var correctSelectedTickets = "";
                            function calcSum(key, val)
                            {

                                sumqty = 0;
                                sumprc = 0;

                                data[key].quantity = val;


                                for (count in data)
                                {
                                    sumprc += (Number(data[count].price)) * (Number(data[count].quantity));
                                    sumqty += Number(data[count].quantity);
                                }

                                $('#totalQty').val(sumqty);
                                $('#totalPrc').val(sumprc);
                                // alert("result" + sumprc);
                                document.getElementById('totalPrice').value = "" + sumprc + " KD";
                                //alert(data);
                                // document.getElementById('ticketTypes').value = data;
                                //alert("result 2" + sumprc);
                                document.getElementById('total').value = "" + sumprc + ".00 KD";
                                //alert("Total" + sumprc);

                            }

                            function checkData()
                            {

                                errorString = null;
                                document.getElementById('errorMessage').value = "";


                                if (document.getElementById("avPackages").value == 'none')
                                {
                                    errorString = "Please choose package";


                                }

                                if (sumqty == 0)
                                {
                                    errorString = "You didnt choose any ticket";


                                }

                                if (<%=errMessage%> != null)
                                {
                                    errorString = "<%=errMessage%>";
                                }

                                if (validate() == false)
                                {

                                    errorString = "Wrong email format";
                                }

                                if (document.getElementById("payment-type-knet").checked == false && document.getElementById("payment-type-cash").checked == false)
                                {

                                    errorString = "Please choose payment type (KNET - Cash)";
                                }
                                if (errorString != null)
                                {

                                    document.getElementById('errorMessage').value = errorString;
                                    return false;
                                    //var isValidEmail = formEmail.checkValidity();
                                } else
                                {

                                    selectedTickets = "[";
                                    details = "[";

                                    for (var key in data)
                                    {
                                        if (data[key].quantity)
                                        {
                                            selectedTickets += "{\"ticket_id\":" + data[key].ticket_id;
                                            selectedTickets += ",\"quantity\":" + data[key].quantity;
                                            selectedTickets += ",\"package_id\":" + data[key].package_id;
                                            selectedTickets += "},";

                                            details += "{\"age_group_name\":\"" + data[key].age_group_name + "\"";
                                            details += ",\"quantity\":" + data[key].quantity;
                                            details += "},";

                                        }
                                    }


                                    correctSelectedTickets = selectedTickets.substring(0, selectedTickets.lastIndexOf(",")) + "]";
                                    correctDetails = details.substring(0, details.lastIndexOf(",")) + "]";
                                    document.getElementById('ticketTypes').value = correctSelectedTickets;
                                    // alert("correctSelectedTickets : " + correctSelectedTickets);
                                    // alert("correctDetails : " + correctDetails);
                                    document.getElementById('detail').value = correctDetails;
                                    return true;
                                }


                            }

                            $('#bookButton').on('click', function (event) {

                                if (document.getElementById("payment-type-knet").checked == true)
                                {

                                    payment_method = "knet";
                                    
                                } else
                                {
                                    payment_method = "cash";
                                }
                                document.getElementById('pay_method').value = payment_method;
                                // alert("payment method"+payment_method)
                                token = "<%=logintoken%>";
                                document.getElementById('token').value = token;
                            });


                            function validateEmail(email) {
                                if (document.getElementById('formEmail').value == "")
                                {
                                    document.getElementById('formEmail').value = "any@kmt.kw";
                                    return true;
                                } else
                                {
                                    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                                    return re.test(email);
                                }
                            }

                            function validate() {

                                var email = $("#formEmail").val();


                                if (validateEmail(email)) {
                                    return true;
                                } else {
                                    return false;
                                }

                            }


 function changeColorTickType1() {
     
     var radio = document.getElementsByClassName('TickType1');
     radio[0].style.border = 'whitesmoke';
     radio[0].style.color = '#fff';
     radio[0].style.backgroundColor = '#121B3C';//red
      var radio = document.getElementsByClassName('TickType2');
      radio[0].style.border = 'whitesmoke';
      radio[0].style.color = '#fff';
             radio[0].style.backgroundColor = '#D20806';//blue
             

 }
 
 function changeColorTickType2() {
    
     var radio = document.getElementsByClassName('TickType2');
     radio[0].style.border = 'whitesmoke';
     radio[0].style.color = '#fff';
     radio[0].style.backgroundColor = '#121B3C';//red
  
      var radio = document.getElementsByClassName('TickType1');
      radio[0].style.border = 'whitesmoke';
      radio[0].style.color = '#fff';
             radio[0].style.backgroundColor = '#D20806';//blue
              

 }

function changeColorPayType1() {
   
     var radio = document.getElementsByClassName('PayType1');
     radio[0].style.border = 'whitesmoke';
     radio[0].style.color = '#fff';
     radio[0].style.backgroundColor = '#121B3C';
    
      var radio = document.getElementsByClassName('PayType2');
      radio[0].style.border = 'whitesmoke';
      radio[0].style.color = '#fff';
             radio[0].style.backgroundColor = '#D20806';
             

 }
 
 function changeColorPayType2() {
    
     var radio = document.getElementsByClassName('PayType2');
     radio[0].style.border = 'whitesmoke';
     radio[0].style.color = '#fff';
     radio[0].style.backgroundColor = '#121B3C';
     
 
      var radio = document.getElementsByClassName('PayType1');
      radio[0].style.border = 'whitesmoke';
      radio[0].style.color = '#fff';
             radio[0].style.backgroundColor = '#D20806';
           
             

 }

 document.onreadystatechange = function () {
    
  var state = document.readyState
  if (state == 'interactive') {
       document.getElementById('pageContent').style.visibility="hidden";
  } else if (state == 'complete') {
      setTimeout(function(){
         document.getElementById('interactive');
         document.getElementById('load').style.visibility="hidden";
         document.getElementById('pageContent').style.visibility="visible";
      },1000);
  }
}
  
 /* jQuery(document).ready(function ($) {
      alert("page start loading");
    $(window).load(function () {
        setTimeout(function(){
            $('#preloader').fadeOut('slow', function () {
            });
        },10000); // set the time here
        
    }); 
      alert("page end loading");
});*/
/* $(window).on('load', function(){
  setTimeout(removeLoader, 2000); //wait for page load PLUS two seconds.
});
function removeLoader(){
    $( "#loadingDiv" ).fadeOut(500, function() {
      // fadeOut complete. Remove the loading div
      $( "#loadingDiv" ).remove(); //makes page more lightweight 
  });  
}*/
        </script>
    </body>
</html>
