<%-- 
    Document   : printTicket
    Created on : Aug 28, 2019, 2:54:46 PM
    Author     : masiya
--%>

<%@page import="java.io.IOException"%>
<%@page import="java.nio.file.DirectoryNotEmptyException"%>
<%@page import="java.nio.file.Paths"%>
<%@page import="java.nio.file.Files"%>
<%@page import="java.nio.file.NoSuchFileException"%>
<%@page import="java.net.URL"%>
<%@page import="com.api.TicketPrintedData"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.api.ErrorKmt"%>
<%@page import="com.api.LocalProperties"%>
<%@page import="com.api.PrintDetails"%>
<%@page import="com.api.ApiHandler"%>
<%@page import="com.api.TicketType"%>
<%@page import="com.fasterxml.jackson.core.type.TypeReference"%>
<%@page import="com.api.BookingData"%>
<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%>
<%@page import="java.util.Properties"%>
<%@page import="com.api.BookingDataDetails"%>
<%@page import="javax.imageio.ImageIO"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%@page import="java.awt.Color"%>
<%@page import="javax.swing.JLabel"%>
<%@page import="java.awt.Graphics2D"%>
<%@page import="java.awt.image.BufferedImage"%>
<%@page import="java.awt.Graphics"%>
<%@page import="java.awt.print.Printable"%>
<%@page import="org.apache.maven.doxia.docrenderer.DocumentRenderer"%>
<%@page import="javax.swing.text.html.HTMLDocument"%>
<%@page import="javax.swing.text.html.HTMLEditorKit"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="org.xhtmlrenderer.simple.XHTMLPrintable"%>
<%@page import="java.io.File"%>
<%@page import="org.xhtmlrenderer.simple.XHTMLPanel"%>
<%@page import="java.awt.print.Paper"%>
<%@page import="java.awt.print.PageFormat"%>
<%@page import="java.awt.print.PrinterJob"%>
<%@page import="java.awt.print.PrinterException"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="com.google.gson.GsonBuilder"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
if (session.getAttribute("user")==null)
{
      String errMessage = "session is expired.";
       response.setHeader("Pragma","no-cache");
response.setHeader("Cache-Control","no-store");
response.setHeader("Expires","0");
response.setDateHeader("Expires",-1);
               session.setAttribute("user", null);
                session.invalidate();
                
               // RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
               // rd.forward(request, response);
                //response.reset();
               // request.logout();
                response.sendRedirect("relogin.jsp?errMessage=" + errMessage);
    /* String see =  request.getContextPath();
     String See2=request.getPathInfo();
     String Seer3=request.getRequestURI();
     String Seer4=request.getServletPath();
    
      //response.sendRedirect("index.jsp?errMessage=" + errMessage);
       RequestDispatcher rd = request.getRequestDispatcher("relogin.jsp?errMessage=" + errMessage);
       rd.forward(request, response);*/
        
                                   
}

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>KMT Point Of Sale</title>
    </head>
    <body>
        <form  id="printTickets" method="post" >


            <div id="par"  class="my-5 py-4"></div>

            <%

                String printer_name = null;
                int NoOfTickets = 0;
                String ticketJsonStr = "";
                try {

                    LocalProperties local = new LocalProperties();

                    String normalPrinterName = "General_Printer";//local.getString("NormalPrinterName");
                    String VIPPrinterName = "VIP_Printer";//local.getString("VIPPrinterName");
                    String booking_id = (String) request.getParameter("bookId");
                    printer_name = normalPrinterName;
                    ApiHandler handler = new ApiHandler();
                    Properties params = new Properties();
                    String jsonBookDetailsStr = null;

                    params.put("book_id", booking_id);
                    jsonBookDetailsStr = handler.callAPI(params, "/form/event/book?book_id=" + booking_id, "BookingDetails");

                    request.setAttribute("booking_id", booking_id);

                    ObjectMapper bookMapper = new ObjectMapper();
                    TypeReference<BookingData> bookType = new TypeReference<BookingData>() {
                    };
                    BookingData jsonBookDetail = bookMapper.readValue(jsonBookDetailsStr, bookType);

                    ErrorKmt error = jsonBookDetail.getError();
                     if (error != null) {
                     int errorNo = error.getCode();
                     String errMessage = error.getMessage();
                     response.sendRedirect("errorAll.jsp?errorNo=" + errorNo + "errMessage=" + errMessage);

                     } else {
                    //number of tickets
                    ArrayList<TicketPrintedData> AllTicket = new ArrayList<TicketPrintedData>();

                    BookingDataDetails bDetails = jsonBookDetail.getData();
                    String eventTitle = bDetails.getTitle();
                    String eventLocName = bDetails.getLocation_name();
                    int PosEmployeeID = bDetails.getEmployee_id();
                    TicketType tickets[] = bDetails.getTickets();
                    String ticketType = tickets[0].getType_name();
                    String validityDate = "";
                    int noOfDays = 0;
                    for (int counter = 0; counter < bDetails.getPackage_dates().length; counter++) {
                        validityDate += bDetails.getPackage_dates()[counter] + "   ";
                        noOfDays++;
                    }
                    if (ticketType.equals("VIP")) {
                        printer_name = VIPPrinterName;
                    } else {
                        ticketType = "General";
                    }

                    TicketPrintedData tempTicket = null;
                    for (int counter = 0; counter < tickets.length; counter++) {

                        for (int counter2 = 0; counter2 < tickets[counter].getQuantity(); counter2++) {
                            tempTicket = new TicketPrintedData();
                            tempTicket.setEventTitle(eventTitle);
                            tempTicket.setEventLocName(eventLocName);
                            tempTicket.setPosEmployeeID("" + PosEmployeeID + "");
                            tempTicket.setPrinterName(printer_name);
                            tempTicket.setTicket_type_name(ticketType);
                            tempTicket.setTicket_age_group_name(tickets[counter].getAge_group_name());
                            tempTicket.setBooking_id(booking_id);

                            float ticket_price = tickets[counter].getPrice();
                            if (ticket_price == 0) {
                                tempTicket.setTicket_price("Free");
                            } else {
                                request.setAttribute("ticket_price", "" + ticket_price + "00");
                                tempTicket.setTicket_price("" + ticket_price + "00");
                            }
                            PrintDetails[] ticket_print_details = tickets[counter].getPrint_details();
                            String ticket_id = ticket_print_details[counter2].getPurchased_ticket_id();
                            tempTicket.setTicket_id(ticket_id);
                            String ticket_qrcode_url = ticket_print_details[counter2].getQrcode_url();

                        //String ticket_qrcode_url_ = ticket_qrcode_url.replace("//", "$");
                            //////// tempTicket.setTicket_qrcode_url("qrcode"+ticket_id+".png");
                            tempTicket.setTicket_qrcode_url(ticket_qrcode_url);
                        //tempTicket.setTicket_qrcode_url("https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=3670-2232-15&chld=L|1&choe=UTF-8");

                       // String validity_date = ticket_print_details[counter2].getValidity_date();
                            //tempTicket.setValidity_date(validity_date);
                            tempTicket.setValidity_date(validityDate);
                            
                            tempTicket.setNoOfDays(""+noOfDays);

                            // app.printPage(bookigDataProp,printer_name);
                            AllTicket.add(tempTicket);

                        }
                    }

                    NoOfTickets = AllTicket.size();

                    String ticketJsonStr_ = new Gson().toJson(AllTicket);
                    ticketJsonStr = ticketJsonStr_.replace("\"", "\\\"");
                     }
                } catch (Exception e) {
                    String errorMsg = "Internal error please contact system adminstrator";

                    response.sendRedirect("errorAll.jsp?errMessage=" + errorMsg + "&errorDetails=" + e.toString());
                }
                
                String path = getServletContext().getRealPath("/");

            %>



        </form>

        <script>

            

            if (document.getElementById("pApplet"))
            {

                document.getElementById("par").removeChild(document.getElementById("pApplet"));
            }

            var tApplet = document.createElement('applet');
            tApplet.code = "Print";
            tApplet.archive = "PrintDetailApplet.jar";
            tApplet.id = "pApplet";
            tApplet.width = "1";
            tApplet.height = "1";
            var NoOfTickets = "<%=NoOfTickets%>";
            //alert("NoOfTickets" + NoOfTickets);
            tApplet.innerHTML = "<PARAM name='NoOfTickets' value='" + NoOfTickets + "'>"+
                  "<PARAM name='code' value='Print'>" +
                  "<PARAM name='archive' value='PrintDetailApplet.jar'>";

            var ticketsAsString = "<%=ticketJsonStr%>";
        //alert("ticketsAsString" + ticketsAsString );
            var arr = JSON.parse(ticketsAsString)
            for (var count = 0; count < NoOfTickets; count++)
            {


                tApplet.innerHTML += '   <PARAM name="printerName_' + count + '"value="' + arr[count].printerName + '">' +
                        ' <PARAM name="booking_id_' + count + '" value="' + arr[count].booking_id + '">' +
                        ' <PARAM name="eventTitle_' + count + '" value="' + arr[count].eventTitle + '">' +
                        ' <PARAM name="eventLocName_' + count + '" value="' + arr[count].eventLocName + '">' +
                        ' <PARAM name="PosEmployeeID_' + count + '" value="' + arr[count].PosEmployeeID + '">' +
                        ' <PARAM name="ticket_type_name_' + count + '" value="' + arr[count].ticket_type_name + '">' +
                        ' <PARAM name="ticket_age_group_name_' + count + '" value="' + arr[count].ticket_age_group_name + '">' +
                        ' <PARAM name="ticket_price_' + count + '" value="' + arr[count].ticket_price + '">' +
                        ' <PARAM name="ticket_id_' + count + '" value="' + arr[count].ticket_id + '">' +
                        ' <PARAM name="ticket_qrcode_url_' + count + '" value="' + arr[count].ticket_qrcode_url + '">' +
                        ' <PARAM name="validity_date_' + count + '" value="' + arr[count].validity_date + '">'+
                        ' <PARAM name="no_of_days_' + count + '" value="' + arr[count].noOfDays + '">';
        //alert("count :" + arr[count].ticket_qrcode_url +"  :  "+ tApplet.innerHTML );


            }

            document.getElementById("par").appendChild(tApplet);



            //  document.forms["printTickets"].submit();


        </script>


    </body>
</html>
