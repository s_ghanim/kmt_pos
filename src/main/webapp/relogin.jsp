

<html lang="en">
    <head>

        <title>KMT Point Of Sale</title>
        <link rel="icon" href="am.png">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,500,600,700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" >
        <link rel="stylesheet" href="css\main.css">
    </head>




    <body >
      
        <div id="wrapper" class="clearfix">
            <section id="content" class="mt-4" style="background-color: #E7E7E7;">
                <div class="content-wrap mt-4">
                  
                        <div class="container-fluid mt-5 clearfix">
   <!--<div class="mt-4">-->
                            <div  style="max-width: 400px;background-color: transparent;">
                                <div class="card-body">
                                    <div class="alert-section mt-4"></div>
                                    <form action="index.jsp" method="post" >
                                    
        <%
            String errorMessage = (String)request.getParameter("errMessage");
            String errorNo = (String)request.getParameter("errorNo");
            String errorDetails = (String)request.getParameter("errorDetails");
            request.setAttribute("errorNo", errorNo);
            request.setAttribute("errorMessage", errorMessage);
            request.setAttribute("errorDetails", errorDetails);
            
            %>
        
           
          <p class="lead font-weight-bold ">
               <span class="lead font-weight-bold " style="color:navy ">Error : </span>
             <input class="lead text-danger font-weight-bold " type="text"  size="100" id="errorMessage" name="errorMessage" value="${errorMessage}" style="border:none;background-color: #E7E7E7;" disabled>
             <input class="lead  font-weight " type="text"  size="100" id="errorDetails" name="errorDetails" value="${errorDetails}" style="border:none;background-color: #E7E7E7;" disabled>
            
             </p>
             
            <button type="reset" id="loginButton" name="errorButton" class="btn btn-dark skew px-4 mr-5" onclick="backtologin()">



                <span>Back to Login</span>
            </button>
                                           
           


   <!-- <input type='button' id='hideshow2' class='radiobutton' value='Core' onclick="showDiv('#content2')">-->

<div id='content2' class="content2">
</div>

                                    </form>

                              
                        </div>
                    </div>
                </div>




            </section><!-- #content end -->
        </div>
        <!-- Go To Top 	============================================= -->
        <div id="gotoTop" class="icon-angle-up"></div>
  <script>
              
               $(document).ready(function(){
   
      alert("Image successfully loaded.");
   document.getElementById("errorMessage").value="<%=errorMessage%>";
});

         
function backtologin() {
   
  // window.parent.document.getElementById('content2').style.display = "block";
 var ctx = "<%=request.getContextPath()%>";
   window.top.location.href = ctx + "/index.jsp";//submitRedirectUrl;
}
     
             </script>  
  
        <script src="js/jquery.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/functions.js"></script>
        <script src="js/sweet-alert.min.js"></script>
        <script src="js/new-js.js"></script>
       
    </body>
</html>
   

