<%-- 
    Document   : result
    Created on : Sep 9, 2019, 10:49:52 AM
    Author     : masiya
--%>

<%@page import="com.api.Details"%>
<%@page import="com.api.InputData"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.api.TicketType"%>
<%@page import="com.api.TicketDataDetails"%>
<%@page import="com.api.BookingDataDetails"%>
<%@page import="com.api.BookingData"%>
<%@page import="com.api.ErrorKmt"%>
<%@page import="com.fasterxml.jackson.core.type.TypeReference"%>
<%@page import="com.api.Ticket"%>
<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%>
<%@page import="java.util.Properties"%>
<%@page import="com.api.ApiHandler"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
if (session.getAttribute("user")==null)
{
      String errMessage = "session is expired.";
       response.setHeader("Pragma","no-cache");
response.setHeader("Cache-Control","no-store");
response.setHeader("Expires","0");
response.setDateHeader("Expires",-1);
               session.setAttribute("user", null);
                session.invalidate();
                
               // RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
               // rd.forward(request, response);
                //response.reset();
               // request.logout();
                response.sendRedirect("relogin.jsp?errMessage=" + errMessage);
    /* String see =  request.getContextPath();
     String See2=request.getPathInfo();
     String Seer3=request.getRequestURI();
     String Seer4=request.getServletPath();
    
      //response.sendRedirect("index.jsp?errMessage=" + errMessage);
       RequestDispatcher rd = request.getRequestDispatcher("relogin.jsp?errMessage=" + errMessage);
       rd.forward(request, response);*/
        
                                   
}

%>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>KMT Point Of Sale</title>

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,500,600,700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" >
        <link rel="stylesheet" href="css\main.css">
         <style>
   #loading-image {
  position: absolute;
  top: 100px;
  left: 240px;
  z-index: 100;
}
            .grayout {
                opacity: 0.6; 
                filter: alpha(opacity = 60); 
            } 
          #load{
    width:100%;
    height:100%;
    position:fixed;
    z-index:9999;
    background-color: #fff;
    background:"img/kmt_loading.gif"
}
</style>
    </head>
    <body>
  <div id="load">
        
            <img id="loading-image" src="img/kmt_loading.gif" alt="Loading..." />
    </div>
        <form action ="printTicket.jsp?bookId=${bookId}" method="post"> 

            <%
                String result = "";
                String bookId = "";
                String detail = "";
                String detailStr = "";
                String eventId = (String) request.getParameter("eventId");
                String token = (String) request.getParameter("token");
                try {
                    ApiHandler handler = new ApiHandler();
                    Properties params = new Properties();
                    request.setAttribute("eventId", eventId);

                    String firstName = (String) request.getParameter("form-name");
                    params.put("first_name", firstName);

                    String lastName = (String) request.getParameter("last-name");
                    params.put("last_name", lastName);

                    String phoneNumber = (String) request.getParameter("form-phone");
                    params.put("mobile", phoneNumber);
                    params.put("email", (String) request.getParameter("formEmail"));
                    String sendMail = (String) request.getParameter("sendMail");
                    if(sendMail==null)
                    {
                    params.put("sendmail", "no");
                    }else
                    {
                        params.put("sendmail", sendMail);
                    }
                    params.put("token", token);
                    detail = (String) request.getParameter("detail");
                    detailStr = detail.replace("\"", "\\\"");
                    String selectedTickets = (String) request.getParameter("ticketTypes");
//String finalSelectedTickets = selectedTickets.replace("\"", "\\\"");//"[{\"ticket_id\":3274,\"quantity\":1,\"package_id\":3271},{\"ticket_id\":3275,\"quantity\":2,\"package_id\":3271}]"
                    params.put("selected", selectedTickets);//"[{\"ticket_id\":3274,\"quantity\":1,\"package_id\":3271},{\"ticket_id\":3275,\"quantity\":2,\"package_id\":3271}]");  //(String) request.getParameter("ticketTypes"));
                    params.put("event_id", eventId);
                    params.put("employee_id", (String) request.getParameter("employee_id"));

                    String formPaymethod = (String) request.getParameter("pay_method");
                    if (formPaymethod.equals("cash")) {
                        params.put("payment_method", "cod");
                        params.put("payment_type", "cash");
                    } else {
                        params.put("payment_method", "ao_knet");
                        params.put("payment_type", "knet");
                    }

                    String jsonTicketDetailsStr = null;
                    jsonTicketDetailsStr = handler.callAPI(params, "/form/event/", "TicketDetails");

                    ObjectMapper detailMapper = new ObjectMapper();
                    TypeReference<Ticket> detailType = new TypeReference<Ticket>() {
                    };

                    Ticket jsonDetail = detailMapper.readValue(jsonTicketDetailsStr, detailType);
                    ErrorKmt error = jsonDetail.getError();

                    if (error != null) {

                        result = "Error :" + error.getCode() + ":" + error.getMessage();
                        response.sendRedirect("errorAll.jsp?errMessage=" + result);
                    } else {
                        result = "Success";
                        //number of tickets
                        TicketDataDetails data = jsonDetail.getData();
                        bookId = data.getOrder_id();
                        request.setAttribute("customerName", (String) request.getParameter("form-name"));
                        request.setAttribute("customerEmail", (String) request.getParameter("formEmail"));
                        request.setAttribute("customerMobile", (String) request.getParameter("form-phone"));
                        String totPrice = (String) request.getParameter("total");
                        request.setAttribute("totalPrice", totPrice);
                        request.setAttribute("totalQuantity", (String) request.getParameter("totalQty"));

                        if (request.getParameter("ticket-type_vip") != null) {
                            request.setAttribute("ticketType", "VIP");
                        } else {
                            request.setAttribute("ticketType", "General");
                        }

                        TypeReference<ArrayList<Details>> mapType2 = new TypeReference<ArrayList<Details>>() {
                        };
                        ObjectMapper objectMapper1 = new ObjectMapper();
                        ArrayList<Details> jsonT = objectMapper1.readValue(detail, mapType2);

                        int size = jsonT.size();
                        request.setAttribute("detaledData", jsonT);

                        request.setAttribute("bookId", bookId);
                        request.setAttribute("result", result);
                    }
                } catch (Exception e) {
                    String errorMsg = "Internal error please contact system adminstrator";

                    response.sendRedirect("errorAll.jsp?errMessage=" + errorMsg + "&errorDetails=" + e.toString());
                }
            %>
            <input type="hidden" id="eventId" name="eventId" value="${eventId}">

            <div class="modal-dialog modal-lg" role="document">
                <input type="hidden" id="bookId" name="bookId" value="${bookId}">

                <div class="modal-content">
                    <div class="modal-header bg-white d-flex justify-content-start">  
                        <a href="#" data-dismiss="modal" class="text-dark fa-2x"><i class="fas fa-angle-left"></i></a>

                        <h4 class="d-flex flex-column justify-content-center m-0 w-100 text-center">

                            <span class="mt-2"> ${result}</span>
                        </h4>
                    </div>


                    <div class="modal-body">
                        <div class="row pb-5">
                            <div class="col-lg-3 py-3">
                                <ul class="list-unstyled">
                                   <!-- <li class="py-2">
                                        <form id="fr1" >

                                            <button  id="send-mail" type="submit" name="send-mail" class="btn w-100 font-weight-bold btn-outline-danger btn-lg" disabled>send mail</button>
                                        </form>
                                    </li>-->
                                    <li class="py-2">
                                        <a id="printButton" href="printTicket.jsp?bookId=${bookId}" class="btn w-100 font-weight-bold btn-outline-danger btn-lg" >Print</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-9 pl-lg-5 font-weight-bold py-3">
                                <ul id="deta" class="list-unstyled">



                                    <li>${ticketType}</li>
                                    <li>Order: ${bookId}</li>
                                    <li>Customer Name: ${customerName}</li>
                                    <li>Customer Email: ${customerEmail}</li>
                                    <li>Customer Mobile: ${customerMobile}</li>
                                    <li>Number Of Tickets: ${totalQuantity}</li>
                                    <li>Total: ${totalPrice}</li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <script>
            //alert("script began");
            var s = "<%=detailStr%>";
            //alert(s);
            var data = JSON.parse(s);

            for (var key in data) {
                var ticketEle = document.createElement('li');

                ticketEle.id = 'tickets';
                ticketEle.innerHTML = 'Category :' + data[key].age_group_name + ' &emsp;&emsp; Quantity :' + data[key].quantity;
                // alert(ticketEle.innerHTML);
                document.getElementById("deta").appendChild(ticketEle);


            }
            
            document.onreadystatechange = function () {
    
  var state = document.readyState
  if (state == 'interactive') {
       document.getElementById('pageContent').style.visibility="hidden";
  } else if (state == 'complete') {
      setTimeout(function(){
         document.getElementById('interactive');
         document.getElementById('load').style.visibility="hidden";
         document.getElementById('pageContent').style.visibility="visible";
      },1000);
  }
}
  
        </script>
    </body>
</html>
