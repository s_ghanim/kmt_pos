<%-- 
    Document   : cancelEvent
    Created on : Sep 9, 2019, 2:41:10 PM
    Author     : masiya
--%>

<%@page import="java.util.Properties"%>
<%@page import="com.api.ErrorKmt"%>
<%@page import="com.fasterxml.jackson.core.type.TypeReference"%>
<%@page import="com.api.Ticket"%>
<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%>
<%@page import="com.api.ApiHandler"%>
<%@page import="com.api.CancelOrder"%>
<%@page import="com.api.CancelOrderDetails"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            ApiHandler handler = new ApiHandler();
            Properties params = new Properties();
            String eventID = (String) request.getParameter("eventId");
            params.put("username", (String) request.getParameter("username"));
           
            params.put("password", (String) request.getParameter("password"));
            params.put("book_id", (String) request.getParameter("book_id"));
            String jsonCancelStr = null;
            jsonCancelStr = handler.callAPI(params, "events/order/cancel", "CancelOrder");

                //String newjsonTicketDetailsStr = jsonTicketDetailsStr.toString().substring(jsonTicketDetailsStr.toString().indexOf("data\"") + 6);
               // String newjsonTicketDetailsStr2 = newjsonTicketDetailsStr.substring(0, newjsonTicketDetailsStr.length() - 1);
            //String newjsonResponse1 = newjsonResponse.substring(0, newjsonResponse.length() - 1);
            ObjectMapper detailMapper = new ObjectMapper();
            TypeReference<CancelOrder> detailType = new TypeReference<CancelOrder>() {
            };
            String bookId = "";
            CancelOrder jsonDetail = detailMapper.readValue(jsonCancelStr, detailType);
            ErrorKmt error = jsonDetail.getError();
            int errorNo = error.getCode();
            String result = "";
            if (errorNo != 0) {
                result = error.getMessage();
                
            } else {
                CancelOrderDetails data = jsonDetail.getData();
                result = data.getStatus_message();
                
            }
            
            %>
    </body>
</html>
