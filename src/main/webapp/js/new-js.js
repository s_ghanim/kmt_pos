/**
 * Make Option
 * 
 * @param value 
 * @param option 
 * @param attr 
 * @param attrVal 
 */



/**
 * Validate Form
 * 
 * @param selector 
 */
function validateForm(selector){
    var errors      = [],
        inputVal    = '',
        inputName   = '',
        inputs      = $(selector).find('.required');
    $.each(inputs, function(index, ele){
        inputVal    = $(this).val();
        inputName   = $(this).attr('name');
        if(inputVal == ''){
            errors.push(inputName);
            $(this).addClass('has-error');
        }
    });
    if(errors.length > 0){
        return false;
    }
    return true;
}

/**
 * SHOW SUCCES FLOW STRAT
 * 
 * @param result 
 * @param section 
 */
function successFlow(result, section) { 
    $('.section-alert').empty();
    section.find('.section-alert').addClass('alert alert-success').text(result.message);            
    section.find('form').reset();
}

/**
 * SHOW ERROR FLOW STRAT
 * 
 * @param result 
 * @param section 
 */
function errorFlow(result, section) {
    $('.section-alert').empty();
    if(result.errors.length != 0){
        section.find('.alert-section').addClass('alert alert-danger');
        var htmlErors   = "<ul>";        
        $.each(result.errors, function (indexInArray, valueOfElement) { 
            section.find('input[name='+ indexInArray +']').addClass('has-error');
            htmlErors   += "<li>"+ valueOfElement +"</li>";        
        });
        htmlErors   += "</ul>";
        section.find('.alert-section').append(htmlErors);
    }    
}

/*** REMOVE HAS ERROR STRAT ***/
$(document).on('focus', '.has-error', function () {
    var thisInput   = $(this),
        thisForm    = thisInput.parents('form'),
        inputName   = thisInput.attr('name');

    thisInput.removeClass('has-error');
    thisForm.find('.validation-error.'+ inputName).empty();
});