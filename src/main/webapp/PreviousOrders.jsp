<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KMT POS</title>
    <link rel="icon" href="am.png">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,500,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" >
    <link rel="stylesheet" href="css\main.css">
  </head>
  <!-- This Page is Developed By M.Ibrahim -->
  <!-- URl: fiverribrahm.000webhostapp.com -->
  <!-- Profile: fiverr.com/ibrahm -->
  <body class="text-dark">
    <header>
      <nav class="navbar navbar-expand-lg navbar-dark bg-danger  pl-3">
        <a class="navbar-brand" href="#"><img src="img/logo.png" class="img-fluid" alt=""></a>
        <h3><font color='white'>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Development</font></h3>
           
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar"  aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse h-100" id="navbar">
          <ul class="navbar-nav ml-lg-5 pl-lg-5 mr-auto d-flex justify-content-center">
            <li class="posr m-0 d-none d-lg-inline-block">
              <img src="img/line.png" class="posa" alt="">
            </li>
            <li class="nav-item ">
              <a class="nav-link px-2" href="#">Visits</a>
            </li>
            <li class="nav-item">
              <a class="nav-link px-2" href="#">Experiences</a>
            </li>
            <li class="nav-item">
              <a class="nav-link px-2" href="#">Events</a>
            </li>
            <li class="nav-item">
              <a class="nav-link px-2" href="#">Our Tracks</a>
            </li>
            <li class="nav-item">
              <a class="nav-link px-2" href="#">Business & VIP</a>
            </li>
            <li class="nav-item">
              <a class="nav-link px-2" href="#">Buy</a>
            </li>
          </ul>
          <ul class="navbar-nav d-flex nav1  ml-auto d-flex justify-content-center">
            <li class="nav-item fst posr">
              <a class="nav-link px-2 py-lg-4" href="#">
                <img src="img/line.png" class="posa d-none d-lg-block" alt="">
                <i class="fas fa-search"></i>
                <span class="d-inline-block d-lg-none">Search</span>
              </a>
            </li>
            <li class="nav-item posr last m-0">
              <img src="img/btn-back.png" class="posa d-none d-lg-block" alt="">
              <a class="nav-link  py-lg-4 pl-lg-4 pr-lg-5 pl-3" href="#"><span class="pl-lg-5">Visits</span></a>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-5 pt-lg-5 px-0 d-flex flex-column">
            <div class="pr-lg-5 pt-4 order-2 order-lg-1 pr-md-3 pb-lg-4 mx-lg-5 px-3">
              <h3 class="text-uppercase">Previous orders</h3>
              <ul class="nav nav-tabs flex-column">
                <li class="nav-item">
                  <a class="nav-link active text-dark p-0 m-0 my-2" data-toggle="tab" href="#fst">
                    <div class="event font-weight-bold shadow fz-14">
                      <div class="head bg-white pr-md-5 px-3 py-2 text-uppercase">
                        <p class=" m-0">DUCATI 4U - MOTORCYLE DRIVER TRAINING</p>
                      </div>
                      <div class="info bg-light px-3 py-2 text-danger d-flex justify-content-between">
                        <p class="m-0">June 7, 2019- 4am</p>
                        <p class="m-0">10KWD</p>
                      </div>
                    </div>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link p-0 m-0 my-2 text-dark" data-toggle="tab" href="#snd">
                    <div class="event font-weight-bold shadow fz-14">
                      <div class="head bg-white pr-md-5 px-3 py-2 text-uppercase">
                        <p class=" m-0">DUCATI 4U - MOTORCYLE DRIVER TRAINING</p>
                      </div>
                      <div class="info bg-light px-3 py-2 text-danger d-flex justify-content-between">
                        <p class="m-0">June 7, 2019- 4am</p>
                        <p class="m-0">10KWD</p>
                      </div>
                    </div>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link p-0 m-0 my-2 text-dark" data-toggle="tab" href="#trd">
                    <div class="event font-weight-bold shadow fz-14">
                      <div class="head bg-white pr-md-5 px-3 py-2 text-uppercase">
                        <p class=" m-0">DUCATI 4U - MOTORCYLE DRIVER TRAINING</p>
                      </div>
                      <div class="info bg-light px-3 py-2 text-danger d-flex justify-content-between">
                        <p class="m-0">June 7, 2019- 4am</p>
                        <p class="m-0">10KWD</p>
                      </div>
                    </div>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link text-dark p-0 m-0 my-2" data-toggle="tab" href="#frt">
                    <div class="event font-weight-bold shadow fz-14">
                      <div class="head bg-white pr-md-5 px-3 py-2 text-uppercase">
                        <p class=" m-0">DUCATI 4U - MOTORCYLE DRIVER TRAINING</p>
                      </div>
                      <div class="info bg-light px-3 py-2 text-danger d-flex justify-content-between">
                        <p class="m-0">June 7, 2019- 4am</p>
                        <p class="m-0">10KWD</p>
                      </div>
                    </div>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link p-0 m-0 my-2 text-dark" data-toggle="tab" href="#fft">
                    <div class="event font-weight-bold shadow fz-14">
                      <div class="head bg-white pr-md-5 px-3 py-2 text-uppercase">
                        <p class=" m-0">DUCATI 4U - MOTORCYLE DRIVER TRAINING</p>
                      </div>
                      <div class="info bg-light px-3 py-2 text-danger d-flex justify-content-between">
                        <p class="m-0">June 7, 2019- 4am</p>
                        <p class="m-0">10KWD</p>
                      </div>
                    </div>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link p-0 m-0 my-2 text-dark" data-toggle="tab" href="#six">
                    <div class="event font-weight-bold shadow fz-14">
                      <div class="head bg-white pr-md-5 px-3 py-2 text-uppercase">
                        <p class=" m-0">DUCATI 4U - MOTORCYLE DRIVER TRAINING</p>
                      </div>
                      <div class="info bg-light px-3 py-2 text-danger d-flex justify-content-between">
                        <p class="m-0">June 7, 2019- 4am</p>
                        <p class="m-0">10KWD</p>
                      </div>
                    </div>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-lg-7 pt-lg-5 pt-3 pl-lg-5 bg-white">
            <div class="tab-content h-100">
              <div class="tab-pane h-100 fade show active" id="fst">
                <div class="d-flex h-100 justify-content-between flex-column">
                  <form action="" class="form-group">
                    <div class="row mt-4">
                      <div class="col-lg-7 col-md-10">
                        <p class="small text-secondary font-weight-bold">Please add more details for the refund</p>
                        
                      </div>
                    </div>         
                    <p class="m-0 mt-4">
                      <button class="btn btn-outline-danger text-capitalize fz-37 pb-1 pt-0 btn-lg font-weight-bold px-4" >Book</button>
                    </p>
                  </form>
                  <form action="" class="form-group m-0">
                    <div class="row mt-5">
                      <div class="col-md-6 pt-lg-5">
                        <p class="font-weight-bold small text-secondary">Please add more details for the refund</p>
                        <p class="m-0 mt-4 mb-3">
                          <button class="btn btn-outline-danger text-capitalize fz-37 pb-1 pt-0 btn-lg font-weight-bold px-4">Cancel</button>
                        </p>
                      </div>
                      <div class="col-md-6 bg-light py-5 px-4 px-lg-5">
                        <h4 class="text-dark mb-3">Supervisor account</h4>
                        <input type="text" id ="username" class="form-control bg-transparent border-secondary br-30 mb-2" placeholder="Username or email address">
                        <input type="Password" id="password" class="form-control bg-transparent border-secondary br-30 mb-2" placeholder="Password">
                        <button class="btn btn-dark skew px-4 mt-2"><span>send</span></button>

                        <div class="my-5 py-4"></div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <div class="tab-pane fade" id="snd">

              </div>
              <div class="tab-pane fade" id="trd">

              </div>
              <div class="tab-pane fade" id="frt">

              </div>
              <div class="tab-pane fade" id="fft">

              </div>
              <div class="tab-pane fade" id="six">

              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" ></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" ></script>
  </body>
</html>
