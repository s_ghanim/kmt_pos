/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.applet.Applet;
import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author masiya
 */
public class Print extends Applet {

    /**
     * Initialization method that will be called after the applet is loaded into
     * the browser.
     */
    int noOfTickets = 0;
    Image img[] = null;
    Image picture2 = null;
    URL url;

    public void init() {
        noOfTickets = Integer.parseInt(this.getParameter("NoOfTickets"));
        img = new Image[noOfTickets];
        for (int counter = 0; counter < noOfTickets; counter++) {
            try {
                url = new URL(this.getParameter("ticket_qrcode_url_" + counter));
                img[counter] = ImageIO.read(url);
            } catch (MalformedURLException ex) {
                Logger.getLogger(Print.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Print.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void paint(Graphics g) {

        for (int counter = 0; counter < noOfTickets; counter++) {
            g.drawImage(img[counter], 30, 30, null);
        }

        String printerName = "General_Printer";

        try {

            Properties bookigDataProp = new Properties();

            printApplet app = new printApplet();
            bookigDataProp = new Properties();
            for (int i = 0; i < noOfTickets; i++) {
                printerName = this.getParameter("printerName_" + i);
                bookigDataProp.setProperty("eventTitle", this.getParameter("eventTitle_" + i));
                bookigDataProp.setProperty("eventLocName", this.getParameter("eventLocName_" + i));
                bookigDataProp.setProperty("PosEmployeeID", this.getParameter("PosEmployeeID_" + i));// this.getParameter("PosEmployeeID"));
                bookigDataProp.setProperty("ticket_type_name", this.getParameter("ticket_type_name_" + i));
                bookigDataProp.setProperty("ticket_age_group_name", this.getParameter("ticket_age_group_name_" + i));
                bookigDataProp.setProperty("ticket_price", this.getParameter("ticket_price_" + i));
                bookigDataProp.setProperty("ticket_id", this.getParameter("ticket_id_" + i));
                bookigDataProp.setProperty("ticket_qrcode_url", "");// this.getParameter("ticket_qrcode_url"));
                bookigDataProp.setProperty("validity_date", this.getParameter("validity_date_" + i));
                bookigDataProp.setProperty("booking_id", this.getParameter("booking_id_" + i));// this.getParameter("validity_date"));
                bookigDataProp.setProperty("no_of_days", this.getParameter("no_of_days_" + i));
                app.printPage(bookigDataProp, printerName, img[i]);

            }

        } catch (Exception e) {
            System.out.println("Exception was thrown. Exception is \t : " + e);

        }
    }
    // TODO overwrite start(), stop() and destroy() methods
}
