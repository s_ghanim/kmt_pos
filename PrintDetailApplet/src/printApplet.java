
//import com.fasterxml.jackson.core.type.TypeReference;
//import com.fasterxml.jackson.databind.ObjectMapper;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import static java.awt.print.Printable.NO_SUCH_PAGE;
import static java.awt.print.Printable.PAGE_EXISTS;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.BufferedReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

/**
 *
 * @author masiya
 */
public class printApplet implements Printable {

    //private static Object PrintUtility;
    private static Properties bookigDataProp = null;
    private static String printerName = null;
    private static StringBuilder sb = null;
    private static BufferedReader br = null;
    private static byte[] array = null;
    private static Image img = null;

    /**
     * @return the printerName
     */
    public static String getPrinterName() {
        return printerName;
    }

    /**
     * @param aPrinterName the printerName to set
     */
    public static void setPrinterName(String aPrinterName) {
        printerName = aPrinterName;
    }

    /**
     * @return the array
     */
    public static byte[] getArray() {
        return array;
    }

    /**
     * @param aArray the array to set
     */
    public static void setArray(byte[] aArray) {
        array = aArray;
    }

    /**
     * @return the bookigDataProp
     */
    public static Properties getBookigDataProp() {
        return bookigDataProp;
    }

    /**
     * @param aBookigDataProp the bookigDataProp to set
     */
    public static void setBookigDataProp(Properties aBookigDataProp) {
        bookigDataProp = aBookigDataProp;
    }

    /**
     * @return the img
     */
    public static Image getImg() {
        return img;
    }

    /**
     * @param aImg the img to set
     */
    public static void setImg(Image aImg) {
        img = aImg;
    }

    private Object data;

    public static PrintService findPrintService(String printerName) {

        PrintService service = null;

        // Get array of all print services - sort order NOT GUARANTEED!
        PrintService[] services = PrinterJob.lookupPrintServices();

        // Retrieve specified print service from the array
        for (int index = 0; service == null && index < services.length; index++) {

            if (services[index].getName().equalsIgnoreCase(printerName)) {

                service = services[index];
            }
        }

        // Return the print service
        return service;
    }

    /**
     * Retrieve a PrinterJob instance set with the PrinterService using the
     * printerName.
     *
     * @return
     * @throws Exception IllegalStateException if expected printer is not found.
     */
    public static PrinterJob findPrinterJob(String printerName) throws Exception {

        // Retrieve the Printer Service
        PrintService printService = printApplet.findPrintService(printerName);

        // Validate the Printer Service
        if (printService == null) {

            throw new IllegalStateException("Unrecognized Printer Service \"" + printerName + '"');
        }

        // Obtain a Printer Job instance.
        PrinterJob printerJob = PrinterJob.getPrinterJob();

        // Set the Print Service.
        printerJob.setPrintService(printService);

        // Return Print Job
        return printerJob;
    }

    public void printPage(Properties bookigDataProp, String printer_name, Image imge) throws PrinterException, Exception {

        PrinterJob job = printApplet.findPrinterJob(printer_name);
        printApplet.setImg(imge);
        printApplet.setBookigDataProp(bookigDataProp);
        PageFormat pageFormat2=new PageFormat();
        pageFormat2.setOrientation(PageFormat.REVERSE_LANDSCAPE);
        job.setPrintable(this, pageFormat2 );
        //job.setPrintable(this);
        
        job.print();

    }

    /**
     * Printer list does not necessarily refresh if you change the list of
     * printers within the O/S; you can run this to refresh if necessary.
     */
    public static void refreshSystemPrinterList() {

        Class[] classes = PrintServiceLookup.class.getDeclaredClasses();

        for (int i = 0; i < classes.length; i++) {

            if ("javax.print.PrintServiceLookup$Services".equals(classes[i].getName())) {

                sun.awt.AppContext.getAppContext().remove(classes[i]);
                break;
            }
        }

    }

    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        if (pageIndex != 0) {
            return NO_SUCH_PAGE;
        }
        try {

           
            
            
                  
                    
            Properties testing = printApplet.getBookigDataProp();

            String eventTitle = (String) bookigDataProp.getProperty("eventTitle");
            String eventLocName = (String) bookigDataProp.getProperty("eventLocName");
            String validity_date = (String) bookigDataProp.getProperty("validity_date");
            // String eventTime = (String) bookigDataProp.getProperty("eventTime");
            String PosEmployeeID = (String) bookigDataProp.getProperty("PosEmployeeID");
            String ticket_type_name = (String) bookigDataProp.getProperty("ticket_type_name");
            String ticket_age_group_name = (String) bookigDataProp.getProperty("ticket_age_group_name");
            String ticket_price = (String) bookigDataProp.getProperty("ticket_price");
            String ticket_id = (String) bookigDataProp.getProperty("ticket_id");
            String ticket_qrcode_url = (String) bookigDataProp.getProperty("ticket_qrcode_url");
            String no_of_days = (String) bookigDataProp.getProperty("no_of_days");
            String booking_id = (String) bookigDataProp.getProperty("booking_id");
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date = new Date();
            String ticketTime = formatter.format(date);
            //get data from ticketDetails
            
            graphics.setFont(new Font("Hoefler Text", Font.BOLD, 12));
            graphics.drawString(eventTitle, 85, 220);
            //graphics.drawString("WORLDGT2",120,70);
            graphics.drawString(eventLocName, 85, 240);
            //graphics.drawString("GRANDSTAND", 120, 90);
            if(validity_date.length() > 25)
            {
                graphics.setFont(new Font("Hoefler Text", Font.PLAIN, 8));
           
            }else
            {
            graphics.setFont(new Font("Hoefler Text", Font.PLAIN, 10));
            }
            graphics.drawString(validity_date, 85, 260);
          
            graphics.setFont(new Font("Hoefler Text", Font.PLAIN, 12));
            graphics.drawString("Price", 85, 280);
            graphics.drawString("Name", 145, 280);
            graphics.drawString("KD", 255, 280);
            graphics.setFont(new Font("Hoefler Text", Font.BOLD, 12));
            graphics.drawString(ticket_type_name, 85, 300);
            graphics.drawString(ticket_age_group_name, 145, 300);
            graphics.setFont(new Font("Hoefler Text", Font.BOLD, 12));
            graphics.drawString(ticket_price, 255, 300);

            graphics.setFont(new Font("Hoefler Text", Font.PLAIN, 8));
            graphics.drawString("Ticket No.:", 85, 320);
            graphics.drawString(ticket_id, 123, 320);
            graphics.drawString("Terminal : GP-POS", 190, 320);
            graphics.drawString(PosEmployeeID, 257, 320);

            graphics.drawString("Order No.:", 85, 335);
            graphics.drawString(booking_id, 123, 335);
            graphics.drawString("Sold:", 190, 335);
            graphics.drawString(ticketTime, 210, 335);
            
            if(!no_of_days.equals("1"))
            {
            graphics.drawString("Please keep this ticket with you,it will be used for "+no_of_days+" days", 85, 350);
            }


            Image newImage = printApplet.getImg();

            graphics.drawImage(newImage, 275, 210, 60, 60, null);

        } catch (Exception ex) {
            Logger.getLogger(printApplet.class.getName()).log(Level.SEVERE, null, ex);
        }

        return PAGE_EXISTS;
    }

}
